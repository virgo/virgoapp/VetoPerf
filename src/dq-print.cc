/**
 * @file 
 * @brief Program to access information about online data quality channels.
 * @details Many data quality channels are produced in the DAQ and most of them are saved in the raw data (`/virgoData/ffl/raw.ffl`). The name of these channels starts by `V1:DQ_`. Data quality channels usually take discrete values depending on the data quality. For example, `FLAG` channels take value=1 when the data quality is bad and value=0 when it is good. The `dq-print` command lists data quality channels taking a given value at a given time: the channel is then said to be "active". It can also prints the list of segments when the channels are active.
 *
 * ## Input/Output
 *
 * The `dq-print` command must be given a timing and a list of channels. It prints the list of data quality time segments or the list of active channels.
 *
 * ### Timing
 *
 * The timing is specified as a GPS time interval: `gps-start` &rarr; `gps-end`. This interval can be provided in different ways:
 * - `gps-start=` and `gps-end=` are specified explicitly.
 * - A GPS time of interest `gps=` is specified as well as a (half-)time window `dt=` (by default `dt=5` seconds).
 * - Any combination of the parameters above.
 *
 * ### Channels
 *
 * One or several data quality channels can be provided. If several channels are provided, they will be combined using a OR operation when "segments" are printed.
 *
 * The list of channels can be specified with the `channel=` option. With this option, you can list the channel names separated with a space: `channel="V1:CHANNEL1 V1:CHANNEL2 V1:CHANNEL3"`. You can also use patterns: `channel="V1:DQ_*_TOTO_* V1:DQ_*_TITI_*"`. By default, `channel="V1:DQ_*_FLAG_* V1:DQ*_VETO_*"` is used.
 *
 * Finally, the list of channels can be provided in a text file with `channel-file=`. The channel names or patterns are listed in a single column in this file.
 *
 * A channel is said to be active if it takes a pre-defined value. By default, this value is set to 1. You can change the active value with the `active-value=` option.
 *
 * ### Output
 *
 * The `dq-print` command prints the output in the standard output or in a file which must be specified with the `output=` option.
 *
 * There are 2 types of output:
 * - A first feature provided by `dq-print` is to dump the list of time segments when the data quality channel is active. One or several data quality channels can be combined. To print segments, use the `print="segments"` option in the command line.
 * - Another feature is to print the list of data quality channels which are active. This can be used to study a glitch or to follow-up a gravitational-wave candidate. To activate this feature, use the `print="list-active"` option in the command line.
 *
 * If you use the second feature, only active channels are printed. It is possible to print all channels (active or not) with the `print="list-all"` option. 
 *
 * ## Running `dq-print`
 *
 * Use the following command to run `dq-print`:
 * @verbatim
 dq-print gps=[GPS] channel="V1:CHANNEL1 V1:CHANNEL2 V1:CHANNEL3"
 @endverbatim
 * This commands tests whether `V1:CHANNEL1`, `V1:CHANNEL2`, or `V1:CHANNEL3` is active around a given GPS time.
 *
 * You can also specify a list of channels matching a pattern:
 * @verbatim
 dq-print gps=[GPS] channel="V1:DQ_*_TOTO_* V1:DQ_*_TITI_*"
 @endverbatim
 *
 * Or a file with a list of channels (patterns):
 * @verbatim
 dq-print gps=[GPS] channel-file="myfile.txt"
 @endverbatim
 *
 * ## Options
 *
 * `dq-print` comes with many extra options which can be used at the command line. Just type `dq-print` to get the list of options:
 * - `gps=`: Specify the GPS time of interest.
 * - `gps-start=`: Specify the GPS starting time.
 * - `gps-end=`: Specify the GPS ending time.
 * - `dt=`: Specify the half time window. By default = 5 seconds.
 * - `channel=`: Specify the list of channels (names and/or patterns). By default = `"V1:DQ_*_FLAG_* V1:DQ*_VETO_*"`.
 * - `channel-file=`: Specify the path to a text file listing the channel names and/or patterns.
 * - `ffl=`: Specify the path to the ffl file where to scan data quality channels. By default = `/virgoData/ffl/raw.ffl`
 * - `active-value=`: Specify the active value. By default = 1.
 * - `print=`: Specify the type of output: `"segments"` to print segments when at least one channel is active, `"list-active"` to print the list of active channels, `"list-all"` to print the list of all channels. By default = `"list-active"`
 * - `output=`: Specify the path to the output file. By default, use the standard output.
 * 
 * @snippet this dq-print-usage 
 *
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include <ffl.h>
#include "VPconfig.h"

/**
 * @brief Usage funcion.
 */
int PrintUsage(void){
  cerr<<endl;
  cerr<<"Usage:"<<endl; 
  cerr<<endl;
  //! [dq-print-usage]
  cerr<<"dq-print gps=[GPS time] \\"<<endl;
  cerr<<"             gps-start=[GPS start] \\"<<endl;
  cerr<<"             gps-end=[GPS end] \\"<<endl;
  cerr<<"             dt=[half time window] \\"<<endl;
  cerr<<"             channel=[channel name pattern] \\"<<endl;
  cerr<<"             channel-file=[path to channel file] \\"<<endl;
  cerr<<"             ffl=[path to ffl] \\"<<endl;
  cerr<<"             active-value=[active value] \\"<<endl;
  cerr<<"             print=[print type] \\"<<endl;
  cerr<<"             output=[path to output file]"<<endl;
  cerr<<endl;
  cerr<<"[GPS time]             GPS time of interest"<<endl;
  cerr<<"[half time window]     half time window to be applied on both sides of the GPS time of interest [s]. Default dt=5"<<endl;
  cerr<<"[GPS start]            starting GPS time"<<endl;
  cerr<<"[GPS send]             ending GPS time"<<endl;
  cerr<<endl;
  cerr<<"[channel name pattern] filter channel names to consider. The filter string must be a pattern."<<endl;
  cerr<<"                       Default: channel=\"V1:DQ_*_FLAG_* V1:DQ*_VETO_*\""<<endl;
  cerr<<"[path to channel file] path to the file listing channel names."<<endl;
  cerr<<"                       they must be listed in a text file over a single column."<<endl;
  cerr<<"[path to ffl]          path to ffl (or lcf). Default = /virgoData/ffl/raw.ffl"<<endl;
  cerr<<"[active value]         value for the channel to be said \"active\". Default: val=1"<<endl;
  cerr<<endl;
  cerr<<"[print type]           \"segments\": print segments when at least one channel is active"<<endl;
  cerr<<"                       \"list-active\": print list of active channels"<<endl;
  cerr<<"                       \"list-all\": print list of all channels"<<endl;
  cerr<<"                       default: print=\"list-active\""<<endl;
  cerr<<"[path to output file]  path to output file. By default, use standard output."<<endl;
  //! [dq-print-usage]
  cerr<<endl;
  cerr<<"See also: https://virgo.docs.ligo.org/virgoapp/VetoPerf/dq-print_8cc.html"<<endl;
  cerr<<endl;
  return 1;
}
  
/**
 * @brief Main program.
 */
int main (int argc, char* argv[]){

  // print version
  if(argc>1&&!((string)argv[1]).compare("version")){
    return VP_PrintVersion();
  }

  // number of arguments
  if(argc<2) return PrintUsage();

  // list of parameters + default
  string chfilter = "V1:DQ_*_FLAG_* V1:DQ*_VETO_*"; // channel filter
  double gps_0 = -1.0; // GPS of interest
  double gps_start = -1.0; // GPS start
  double gps_end =-1.0; // GPS end
  double dt = 5.0; // time window
  string fflpath = "/virgoData/ffl/raw.ffl"; // ffl
  double chval = 1.0; // channel value
  string printtype  = "list-active"; // print type
  string outfile = ""; // output file
  string chfile = ""; // channel file

  // loop over arguments
  vector <string> sarg;
  for(int a=1; a<argc; a++){
    sarg=SplitString((string)argv[a],'=');
    if(sarg.size()!=2) continue;
    if(!sarg[0].compare("gps"))           gps_0 = atof(sarg[1].c_str());
    if(!sarg[0].compare("gps-start"))     gps_start = atof(sarg[1].c_str());
    if(!sarg[0].compare("gps-end"))       gps_end = atof(sarg[1].c_str());
    if(!sarg[0].compare("dt"))            dt = atof(sarg[1].c_str());
    if(!sarg[0].compare("channel"))       chfilter = (string)sarg[1];
    if(!sarg[0].compare("channel-file"))  chfile = (string)sarg[1];
    if(!sarg[0].compare("ffl"))           fflpath = (string)sarg[1];
    if(!sarg[0].compare("active-value"))  chval = atof(sarg[1].c_str());
    if(!sarg[0].compare("print"))         printtype = (string)sarg[1];
    if(!sarg[0].compare("output"))        outfile = (string)sarg[1];
  }

  // GPS of interest and time window
  double gps_interest;
  if(gps_0>0){
    gps_interest = gps_0;
    if((gps_start<0) && (gps_end<0) && (dt>0)){ gps_start = gps_0-dt; gps_end = gps_0+dt; }
    else if((gps_start<0) && (gps_end>0) && (dt>0)){ gps_start = gps_interest-dt; }
    else if((gps_start>0) && (gps_end>0) );
    else if((gps_start>0) && (gps_end<0) && (dt>0)){ gps_end = gps_interest+dt; }
    else{
      cerr<<"A valid gps timing must be provided"<<endl;
      cerr<<"Type 'dq-print' for help"<<endl;
      return 1;
    }
  }
  else{
    if((gps_start<0) && (gps_end>0) && (dt>0)){ gps_interest = gps_end-dt; gps_start = gps_interest-dt; }
    else if((gps_start>0) && (gps_end>0)){ gps_interest = gps_start/2.0+gps_end/2.0; }
    else if((gps_start>0) && (gps_end<0) && (dt>0)){ gps_interest = gps_start+dt; gps_end=gps_interest+dt; }
    else{
      cerr<<"A valid gps timing must be provided"<<endl;
      cerr<<"Type 'dq-print' for help"<<endl;
      return 1;
    }  
  }

  // check timing
  if((gps_interest<gps_start) || (gps_interest>gps_end)){
    cerr<<"A valid gps timing must be provided"<<endl;
    cerr<<"Type 'dq-print' for help"<<endl;
    return 1;
  }  

  // print full command
  cout<<"Full command:"<<endl;
  cout<<"\tdq-print gps="<<setprecision(15)<<gps_interest<<" gps-start="<<gps_start<<" gps-end="<<gps_end<<" channel=\""<<chfilter<<"\" channel-file="<<chfile<<" ffl="<<fflpath<<" active-value="<<chval<<" print="<<printtype<<" output="<<outfile<<endl;

  // ffl file
  ffl *ffldata = new ffl(fflpath, "GWOLLUM", 0, ".", 1, 0);
  if(!ffldata->ExtractChannels((unsigned int)gps_start)) return 2;

  // output file (print list only)
  ofstream ofs;
  if((outfile.compare("")) && (printtype.compare("segments"))){
    ofs.open(outfile, ofstream::out);
    if(!ofs.is_open()){
      cerr<<"Output file cannot be open: "<<outfile<<endl;
      return 3;
    }
  }

  // channel file (if any)
  if((chfile.compare("")) && filesystem::is_regular_file(chfile)){
    string chname;
    ReadAscii *R = new ReadAscii(chfile, "s");
    for(unsigned int c=0; c<R->GetNRow(); c++){
      R->GetElement(chname, c, 0);
      chfilter += " "+chname;
    }
    delete R;
  }
  
  // get channel list
  vector <string> channels;
  channels = ffldata->GetChannelList(chfilter);

  // print list of channels
  cout<<"Scanning "<<channels.size()<<" channels..."<<endl;
  for(unsigned int c=0; c<channels.size(); c++) cout<<channels[c]<<" || ";
  cout<<endl;

  // loop over channels
  unsigned int chindex;
  unsigned int samplingrate;
  double chunkduration;
  unsigned int ns; double *vdata;
  double gps_s, gps_e, gps_c;
  double dt_min = -1.0e10;
  Segments *Seg = new Segments();
  for(unsigned int c=0; c<channels.size(); c++){

    // get sampling rate
    samplingrate = ffldata->GetChannelSampling(channels[c], chindex);

    // missing channel
    if(samplingrate<=0){
      if(printtype.compare("segments")){
	if(outfile.compare("")) ofs<<"CH-MISSING \t "<<channels[c]<<endl;
	else cout<<"CH-MISSING \t "<<channels[c]<<endl;
      }
      continue;
    }
    
    // build and print segments
    if(!printtype.compare("segments")){

      // data chunk duration
      if(samplingrate<10.0) chunkduration = 100000.0;
      else if(samplingrate<1000.0) chunkduration = 1000.0;
      else if(samplingrate<10000.0) chunkduration = 100.0;
      else chunkduration = 10.0;
     
      // loop over data chunks
      gps_s = gps_start;
      while(gps_s<gps_end){
	gps_e = TMath::Min(gps_s+chunkduration, gps_end);
	
	// get data
	vdata = ffldata->GetData(ns, channels[c], TMath::Floor(gps_s), TMath::Ceil(gps_e));
	if(ns<=0){ gps_s+=chunkduration; continue; }// no data

	// test data and fill segment list
	for(unsigned int d=0; d<ns; d++)
	  if(vdata[d]==chval)
            Seg->AddSegment(TMath::Floor(gps_s) + (double)d/(double)samplingrate,
                            TMath::Floor(gps_s) + (double)(d+1)/(double)samplingrate);

	// next
	gps_s+=chunkduration;
        delete [] vdata;
      }
    }

    // print channels
    else{
      
      // get data
      vdata = ffldata->GetData(ns, channels[c], TMath::Floor(gps_start), TMath::Ceil(gps_end));
      if(ns<=0){// no data
	if(outfile.compare("")) ofs<<"CH-MISSING \t "<<channels[c]<<endl;
	else cout<<"CH-MISSING \t "<<channels[c]<<endl;
	continue;
      }
      
      dt_min = -1.0e10;

      // test channel value
      for(unsigned int d=0; d<ns; d++){
	gps_c = TMath::Floor(gps_start) + ((double)d+0.5)/(double)samplingrate; // current time
	if((vdata[d]==chval) && (TMath::Abs(gps_interest-gps_c)<TMath::Abs(dt_min))) // get min
	  dt_min = gps_c-gps_interest;
	if(dt_min>-1e10&&gps_c-gps_interest>-dt_min) break;
      }

      // channel is not active
      if(dt_min==-1.0e10){
	if(!printtype.compare("list-all")){
	  if(outfile.compare("")) ofs<<"NOT-ACTIVE \t "<<channels[c]<<endl;
	  else cout<<"NOT-ACTIVE \t "<<channels[c]<<endl;
	}
	delete [] vdata;
	continue;
      }
      
      // check exact value
      if(TMath::Abs(dt_min)<=0.5/(double)samplingrate){
	if(outfile.compare("")) ofs<<showpos<<0.0<<"s \t "<<channels[c]<<endl;
	else cout<<showpos<<0.0<<"s \t "<<channels[c]<<endl;
	delete [] vdata;
	continue;
      }

      // print offset
      if(outfile.compare("")) ofs<<showpos<<dt_min<<"s \t "<<channels[c]<<endl;
      else cout<<showpos<<dt_min<<"s \t "<<channels[c]<<endl;
      delete [] vdata;
      
    }
  }
  
  // print segments
  if(!printtype.compare("segments")){
    
    // trim segments
    Seg->Intersect(gps_start, gps_end);
    
    // print segments
    Seg->Dump(2, outfile);
  }
  
  if(ofs.is_open()) ofs.close();
  
  cout<<"Channel scan is finished"<<endl;
  
  // cleaning
  channels.clear();
  delete Seg;
  delete ffldata;
  
  return 0;
}

