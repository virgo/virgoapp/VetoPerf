//////////////////////////////////////////////////////////////////////////////
//  Author : florent robinet (LAL - Orsay): robinet@lal.in2p3.fr
//////////////////////////////////////////////////////////////////////////////
#include <MakeTriggers.h>
#include <H5Cpp.h>
#include "VPconfig.h"

using namespace std;
using namespace H5;


void PrintUsage(void){
  cerr<<endl;
  cerr<<"Usage:"<<endl;
  cerr<<endl;
  cerr<<"print-pycbc.exe file=[list of files] \\"<<endl;
  cerr<<"                outdir=[path to output directory] \\"<<endl;
  cerr<<"                stream=[stream name] \\"<<endl;
  cerr<<"                time-shift=[time shift in sec]"<<endl;
  cerr<<endl;
  return;
}

int main (int argc, char* argv[]){

  // number of arguments
  if(argc>1&&!((string)argv[1]).compare("version")){
    PrintVersion();
    return 0;
  }

  // number of arguments
  if(argc<2){
    PrintUsage();
    return 1;
  }

  // list of parameters + default
  string filepat         = "";                 // trigger file pattern
  string outdir          =".";                 // output directory
  string streamname      ="V1:TRIGGER_STREAM"; // stream name
  double toffset         = 0.0;                // time offset
  
  // dataset names (hard-coded)
  const int ds_n = 5;
  string ds_name[ds_n] = {"end_time",
			  "snr",
			  "template_duration",
			  "mass1",
			  "mass2"};
  double *ds_data[ds_n];
  			      
  // loop over arguments
  vector <string> sarg;
  for(int a=1; a<argc; a++){
    sarg=SplitString((string)argv[a],'=');
    if(sarg.size()!=2) continue;
    if(!sarg[0].compare("file"))         filepat=(string)sarg[1];
    if(!sarg[0].compare("outdir"))       outdir=(string)sarg[1];
    if(!sarg[0].compare("stream"))       streamname=(string)sarg[1];
    if(!sarg[0].compare("time-shift"))   toffset=atof(sarg[1].c_str());
  }
  
  // check input data
  if(!filepat.compare("")){
    cerr<<"Input trigger files must be provided"<<endl;
    return 1;
  }

  // start Triggers stucture
  MakeTriggers *T = new MakeTriggers(streamname,1);
  T->SetMprocessname("PYCBC");

  // list of files
  string ls;
  ls = "ls -U "+filepat;

  // trigger file looper
  FILE *in;
  in = popen(ls.c_str(), "r");

  //Turn off the auto-printing when failure occurs 
  Exception::dontPrint();

  // locals
  char filename[100000];
  H5File *h5_file;
  Group *h5_group;
  DataSet *h5_dataset[ds_n];
  DataSpace h5_dataspace[ds_n];
  int ds; // dataset index

  // segment list
  vector <string> file_fragments;
  double seg_start, seg_end;
  
  // loop over trigger files
  while(fscanf(in,"%s",filename) != EOF){

    // open file
    cout<<filename<<" ... "<<endl;
    h5_file = new H5File(filename, H5F_ACC_RDONLY);
    
    // missing V1 group
    if(H5Lexists(h5_file->getId(), "/V1", H5P_DEFAULT ) <= 0){
      cout<<"\t missing V1 group"<<endl;
      h5_file->close(); delete h5_file;
      continue;
    }

    // open Virgo group
    h5_group = new Group(h5_file->openGroup("/V1"));

    // test dataset existence
    for(ds=0; ds<ds_n; ds++) if(H5Lexists(h5_group->getId(),ds_name[ds].c_str(),H5P_DEFAULT) <=0) break;
    if(ds!=ds_n){
      cout<<"\t missing trigger dataset"<<endl;
      h5_group->close(); delete h5_group;
      h5_file->close(); delete h5_file;
      continue;
    }

    // get segment from file name
    file_fragments = SplitString(GetFileNameFromPath(filename), '-');
    seg_start = toffset+atof(file_fragments[2].c_str());
    seg_end = toffset+seg_start+atof(file_fragments[3].c_str());
    T->AddSegment(seg_start,seg_end);
    file_fragments.clear();

    // open dataset/space
    for(ds=0; ds<ds_n; ds++){

      // get dataset
      h5_dataset[ds] = new DataSet(h5_group->openDataSet(ds_name[ds].c_str()));

      // get dataspace
      h5_dataspace[ds] = h5_dataset[ds]->getSpace();

      // data vector
      ds_data[ds] = new double [h5_dataspace[ds].getSelectNpoints()];
      h5_dataset[ds]->read(ds_data[ds], PredType::NATIVE_DOUBLE);
    }
    
    //loop over triggers
    for(int t=0; t<h5_dataspace[1].getSelectNpoints(); t++){
      T->AddTrigger(toffset+ds_data[0][t],                 // peak time = t_end
		    ds_data[3][t]+ds_data[4][t],           // frequency = m1+m2
		    ds_data[1][t],                         // SNR
		    1.0,                                   // Q
		    //toffset+ds_data[0][t]-ds_data[2][t],   // tstart = t_end - dur
		    toffset+ds_data[0][t]-0.001,           // tstart = t_end - 0.001
		    toffset+ds_data[0][t]+0.001,           // tend = t_end + 0.001
		    ds_data[4][t],                         // fstart = m1+m2 -m1
		    2.0*ds_data[3][t]+ds_data[4][t],       // fstart = m1+m2 +m1
		    1.0,                                   // amplitude
		    0.0);                                  // phase
    }
    
    // 
    for(ds=0; ds<ds_n; ds++){
      delete ds_data[ds];
      h5_dataspace[ds].close();
      h5_dataset[ds]->close(); delete h5_dataset[ds];
    }
    
    // close group
    h5_group->close(); delete h5_group;
    
    // close file
    h5_file->close(); delete h5_file;

  }
  pclose(in);

  // save trigger file
  T->SortTriggers();
  T->Write(outdir,"root");

  // cleaning
  delete T;
 
  return 0;
}

