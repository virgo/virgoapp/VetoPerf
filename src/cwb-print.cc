//////////////////////////////////////////////////////////////////////////////
//  Author : florent robinet (LAL - Orsay): robinet@lal.in2p3.fr
//////////////////////////////////////////////////////////////////////////////
#include <MakeTriggers.h>
#include "VPconfig.h"

using namespace std;

void PrintUsage(void){
  cerr<<endl;
  cerr<<"Usage:"<<endl;
  cerr<<endl;
  cerr<<"print-cwb.exe file=[path to trigger file] \\"<<endl;
  cerr<<"              outdir=[path to output directory] \\"<<endl;
  cerr<<"              stream=[stream name] \\"<<endl;
  cerr<<"              segments=[path to the segment file]"<<endl;
  cerr<<"              network=[network type]"<<endl;
  cerr<<endl;
  cerr<<"[network type]   \"HV\", \"LV\", \"HLV\"..."<<endl;
  cerr<<endl;
  return;
}

int main (int argc, char* argv[]){

  // number of arguments
  if(argc>1&&!((string)argv[1]).compare("version")){
    PrintVersion();
    return 0;
  }

  // number of arguments
  if(argc<2){
    PrintUsage();
    return 1;
  }

  // list of parameters + default
  string trigfile        = "";                 // trigger file
  string outdir          = ".";                // output directory
  string streamname      = "V1:TRIGGER_STREAM";// stream name
  string segfile         = "";                 // segment file
  string network         = "";                 // network type
		      
  // loop over arguments
  vector <string> sarg;
  for(int a=1; a<argc; a++){
    sarg=SplitString((string)argv[a],'=');
    if(sarg.size()!=2) continue;
    if(!sarg[0].compare("file"))         trigfile=(string)sarg[1];
    if(!sarg[0].compare("outdir"))       outdir=(string)sarg[1];
    if(!sarg[0].compare("stream"))       streamname=(string)sarg[1];
    if(!sarg[0].compare("segments"))     segfile=(string)sarg[1];
    if(!sarg[0].compare("network"))      network=(string)sarg[1];
  }
  
  // check input data
  if(!trigfile.compare("")){
    cerr<<"Input trigger files must be provided"<<endl;
    return 1;
  }
  if(!segfile.compare("")){
    cerr<<"Input segments file must be provided"<<endl;
    return 1;
  }
  if(!network.compare("")){
    cerr<<"A network type must be provided"<<endl;
    return 1;
  }

  // decode network
  string colheader="";
  int virgopos=network.find_first_of('V');
  for(int d=0; d<(int)network.length(); d++) colheader+="d;d;d;";
  if(virgopos==-1){
    cerr<<"Virgo must be part of the network"<<endl;
    return 1;
  }

  // load analysis segments
  Segments *Seg = new Segments(segfile);
  if(!Seg->GetStatus()) return 1;
  cout<<"livetime = "<<Seg->GetLiveTime()<<endl;

  // load trigger files
  ReadAscii *Rtrig = new ReadAscii(trigfile,"s;s;d;d;d;d;d;d;i;d;d;d;i;i;d;i;i;i;"+colheader+"d;d;d");
  if(!Rtrig->GetNRow()) return 1;
  cout<<"number of triggers = "<<Rtrig->GetNRow()<<endl;

  // start Triggers stucture
  MakeTriggers *T = new MakeTriggers(streamname,1);
  T->SetMprocessname("CWB");

  // set segments
  T->Append(Seg);
  delete Seg;

  // read triggers
  double ttime, tdur, tsnr, tamp;
  int tfreq, tband;
  for(int t=0; t<Rtrig->GetNRow(); t++){

    Rtrig->GetElement(ttime,t,18+virgopos);
    Rtrig->GetElement(tsnr,t,18+(int)network.length()+virgopos);
    Rtrig->GetElement(tamp,t,18+2*(int)network.length()+virgopos);
    Rtrig->GetElement(tfreq,t,12);
    Rtrig->GetElement(tband,t,13);
    Rtrig->GetElement(tdur,t,14);
	      
    // save trigger
    T->AddTrigger(ttime,(double)tfreq,sqrt(tsnr),0.0,ttime-tdur/2.0,ttime+tdur/2.0,(double)tfreq-(double)tband/2.0,(double)tfreq+(double)tband/2.0,tamp,0.0);
  }

  // save trigger file
  T->SortTriggers();
  T->Write(outdir,"root");

  // cleaning
  delete T;
  delete Rtrig;
 
  return 0;
}

