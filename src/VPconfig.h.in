/**
 * @file 
 * @brief This module parameterizes a VetoPerf release.
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#ifndef __VPconfig__
#define __VPconfig__

#include <CUtils.h>

using namespace std;

/**
 * @brief Project name.
 */
#define VP_PROJECT_NAME "@PROJECT_NAME@"

/**
 * @brief Project version number.
 */
#define VP_PROJECT_VERSION "@PROJECT_VERSION@"

/**
 * @brief Prints the version number.
 * @details This function also checks the VetoPerf environment:
 * - `$VETOPERF_HTML` must point to an existing directory.
 *
 * @returns 0 if the environment is correctly set.
 */
inline int VP_PrintVersion(void){
  cout<<(string)VP_PROJECT_NAME<<" "<<(string)VP_PROJECT_VERSION<<endl;

  // check OMICRON_HTML
  if(!filesystem::is_directory(getenv("VETOPERF_HTML"))){
    cerr<<"Env: missing $VETOPERF_HTML directory"<<endl;
    return 1;
  }

  return 0;
}

#endif


