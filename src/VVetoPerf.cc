/**
 * @file 
 * @brief See VVetoPerf.h
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "VVetoPerf.h"

ClassImp(VetoPerf)

////////////////////////////////////////////////////////////////////////////////////
Veto::Veto(ffl* aFfl, const string aDefinition, const unsigned int aSnrN){ 
////////////////////////////////////////////////////////////////////////////////////

  vetoffl = aFfl;
  vetodir = ".";
  vetothr = 0.5;
  vetoseg = NULL;

  // veto channel (if FFL is active)
  if(vetoffl!=NULL){
    vetoname = aDefinition;
    vetofile = "";
  }
  // veto file
  else{
    vetoname = GetFileNameFromPath(aDefinition);
    vetofile = aDefinition;
  }
  
  vetodt = 0.0;
  n_seg = 0;
  n_snr = aSnrN;
  n_seguse = new unsigned int [n_snr];
  n_cveto = new unsigned int [n_snr];
  gfreqtime = new TGraph();
  gfreqtime->SetName((vetoname+"_tf").c_str());
  gfreqtime->SetMarkerStyle(8);
  gfreqtime->SetMarkerColor(1);
  gfreqtime->SetMarkerSize(0.8);
  gsnrtime = new TGraph();
  gsnrtime->SetName((vetoname+"_ts").c_str());
  gsnrtime->SetMarkerStyle(8);
  gsnrtime->SetMarkerColor(1);
  gsnrtime->SetMarkerSize(0.8);
  gsnrfreq = new TGraph();
  gsnrfreq->SetName((vetoname+"_fs").c_str());
  gsnrfreq->SetMarkerStyle(8);
  gsnrfreq->SetMarkerColor(1);
  gsnrfreq->SetMarkerSize(0.8);
  for(unsigned int s=0; s<n_snr; s++){
    n_seguse[s] = 0;
    n_cveto[s] = 0;
  }
}

////////////////////////////////////////////////////////////////////////////////////
Veto::Veto(const string aName, Segments* aSegments, const unsigned int aSnrN){ 
////////////////////////////////////////////////////////////////////////////////////

  vetoffl = NULL;
  vetodir = ".";
  vetothr = 0.5;
  vetoname = aName;
  vetoseg = aSegments;
  vetofile="";
  vetodt = 0.0;
  n_seg = 0;
  n_snr = aSnrN;
  n_seguse = new unsigned int [n_snr];
  n_cveto = new unsigned int [n_snr];
  gfreqtime = new TGraph();
  gfreqtime->SetName((vetoname+"_tf").c_str());
  gfreqtime->SetMarkerStyle(8);
  gfreqtime->SetMarkerColor(1);
  gfreqtime->SetMarkerSize(0.8);
  gsnrtime = new TGraph();
  gsnrtime->SetName((vetoname+"_st").c_str());
  gsnrtime->SetMarkerStyle(8);
  gsnrtime->SetMarkerColor(1);
  gsnrtime->SetMarkerSize(0.8);
  gsnrfreq = new TGraph();
  gsnrfreq->SetName((vetoname+"_sf").c_str());
  gsnrfreq->SetMarkerStyle(8);
  gsnrfreq->SetMarkerColor(1);
  gsnrfreq->SetMarkerSize(0.8);
  for(unsigned int s=0; s<n_snr; s++){
    n_seguse[s] = 0;
    n_cveto[s] = 0;
  }
}

////////////////////////////////////////////////////////////////////////////////////
Veto::~Veto(void){ 
////////////////////////////////////////////////////////////////////////////////////
  delete [] n_seguse;
  delete [] n_cveto;
  delete gfreqtime;
  delete gsnrtime;
  delete gsnrfreq;
}

////////////////////////////////////////////////////////////////////////////////////
Segments* Veto::GetSegments(Segments *aInSegments){ 
////////////////////////////////////////////////////////////////////////////////////
  Segments* seg;

  // Use the Segments object
  if(vetoseg!=NULL){
    seg = new Segments(vetoseg->GetStarts(), vetoseg->GetEnds());
    seg->SetTags(-1);
    seg->Intersect(aInSegments);
    return seg;
  }
  
  // veto file
  if(vetoffl==NULL){
    // set tag to 0 --> segment is not used
    seg = new Segments(vetofile, -1);
    seg->Intersect(aInSegments);
    return seg;
  }

  // veto channel
  seg = new Segments();

  // no requested livetime
  if(!aInSegments->GetLiveTime()) return seg;

  // load frame file
  if(!vetoffl->ExtractChannels(aInSegments->GetFirst())) return seg;

  // channel sampling
  unsigned int sampling = vetoffl->GetChannelSampling(vetoname);
  if(sampling==0){
    cerr<<"Veto::GetSegments: cannot extract channel "<<vetoname<<" at "<<aInSegments->GetFirst()<<endl;
    return seg;
  }

  // optimize data chunk duration
  double chunkduration = 1.0;
  if(sampling < 10) chunkduration = 100000.0;
  else if(sampling < 1000) chunkduration = 1000.0;
  else if(sampling < 10000) chunkduration = 100.0;
  else chunkduration = 10.0;
  
  // loop over channel data
  double gstart, gstop;
  unsigned int vsize;
  double *vdata;
  for(unsigned int s=0; s<aInSegments->GetN(); s++){

    // start segment
    gstart = aInSegments->GetStart(s);
    
    // get chunk data
    while(gstart < aInSegments->GetEnd(s)){
      
      gstop=TMath::Min(gstart+chunkduration, aInSegments->GetEnd(s));
      vdata = vetoffl->GetData(vsize, vetoname, gstart, gstop, -999.0);

      // test data
      for(unsigned int d=0; d<vsize; d++){
	if(vdata[d] == -999.0) continue;
	if(vdata[d] >= vetothr)
          // set tag to -1 --> segment is not used
          seg->AddSegment(gstart+(double)d/(double)sampling, gstart+(double)(d+1)/(double)sampling, -1);
      }

      // clean up
      if(vsize>0) delete vdata;
      
      // next chunk
      gstart += chunkduration;
    }
  }
  
  return seg;
}

////////////////////////////////////////////////////////////////////////////////////
VetoPerf::VetoPerf(const vector <double> aSnrThr){ 
////////////////////////////////////////////////////////////////////////////////////

  // timer starts
  time(&timer_start);

  // output directory
  outdir = ".";
  subtitle = "";

  // SNR threshold (sort!)
  if(aSnrThr.size()){
    snrthr = aSnrThr;
    sort(snrthr.begin(), snrthr.end());
  }
  else{
    snrthr.push_back(5.0);
    snrthr.push_back(8.0);
    snrthr.push_back(10.0);
    snrthr.push_back(20.0);
  }
  
  // ffl (none)
  ffldata = NULL;

  // triggers
  TP = NULL;
  n_c_snr = new unsigned int [snrthr.size()];
  for(unsigned int s=0; s<snrthr.size(); s++) n_c_snr[s] = 0;
  hsnr = NULL;
  hfreqtime = NULL;
  gfreqtime = NULL;
  hsnrtime = NULL;
  gsnrtime = NULL;
  hsnrfreq = NULL;
  gsnrfreq = NULL;

  // output
  summary_snrthr = -1;
  printveto = false;
  printvetoclusters = false;

  // input segments
  InSeg = new Segments();
}

////////////////////////////////////////////////////////////////////////////////////
VetoPerf::~VetoPerf(void){
////////////////////////////////////////////////////////////////////////////////////
  cout<<"VetoPerf::~VetoPerf"<<endl;
  delete InSeg;
  delete [] n_c_snr;
  for(unsigned int v=0; v<veto.size(); v++) delete veto[v];
  veto.clear();
  delete hsnr;
  delete hfreqtime;
  delete gfreqtime;
  delete hsnrtime;
  delete gsnrtime;
  delete hsnrfreq;
  delete gsnrfreq;
}

////////////////////////////////////////////////////////////////////////////////////
void VetoPerf::SetTriggers(TriggerPlot* aTriggers, Segments *aInSeg){
////////////////////////////////////////////////////////////////////////////////////

  TP = NULL;
  
  // check trigger plot structure
  if((aTriggers==NULL) || (!aTriggers->GetStatus())){
    cerr<<"VetoPerf::SetTriggers: the input trigger structure is corrupted"<<endl;
    return;
  }
  if(aTriggers->TriggerPlot::GetN()!=snrthr.size()){
    cerr<<"VetoPerf::SetTriggers: the input trigger structure must match the number of SNR thresholds ("<<snrthr.size()<<")"<<endl;
    return;
  }

  // save pointer to triggers
  TP = aTriggers;
  
  // copy input segments
  delete InSeg;
  InSeg = new Segments(TP->GetStarts(), TP->GetEnds());
  if((aInSeg!=NULL) && aInSeg->GetStatus()) InSeg->Intersect(aInSeg);

  // set tag to -1 to intersect with veto segments: see Veto::GetSegments()
  InSeg->SetTags(-1);
    
  // cluster selection
  for(unsigned int c=0; c<TP->GetClusterN(); c++){

    // skip already-excluded clusters
    if(TP->GetClusterTag(c)<0) continue;

    // time selection
    if(!InSeg->IsInsideSegment(TP->GetClusterTime(c))){
      TP->SetClusterTag(c,-1);
      continue;
    }

    // SNR selection
    if(TP->GetClusterSnr(c)<snrthr[0]){
      TP->SetClusterTag(c,-1);
      continue;
    }

    // set to active
    TP->SetClusterTag(c,1);
  }
  
  // make collections
  MakeCollections();
  for(unsigned int s=0; s<snrthr.size(); s++) n_c_snr[s]=TP->GetCollectionN(s);

  // print trigger plots
  gwl_ss<<TP->GetNameConv()<<"-all";
  TP->PrintPlot("freqtime");
  TP->DrawLegend();
  TP->Print(outdir+"/"+gwl_ss.str()+"_freqtime.png");
  TP->PrintPlot("snrtime");
  TP->DrawLegend();
  TP->Print(outdir+"/"+gwl_ss.str()+"_snrtime.png");
  TP->PrintPlot("snrfreq");
  TP->DrawLegend();
  TP->Print(outdir+"/"+gwl_ss.str()+"_snrfreq.png");
  TP->UnDrawLegend();
  TP->UnDrawLegend();
  TP->UnDrawLegend();
  gwl_ss.clear(); gwl_ss.str("");
  
  // get SNR distribution
  delete hsnr;
  hsnr = TP->GetTH1D("snr", 0);
  hsnr->SetName("vp-snr");

  // get distributions (for combined plot)
  delete hfreqtime;
  delete gfreqtime;
  gfreqtime = TP->GetTGraph(&hfreqtime, "freqtime", TMath::Min((UInt_t)TMath::Max(summary_snrthr, 0), (UInt_t)snrthr.size()-1));
  hfreqtime->SetName("vp-freqtime-all-frame");
  gfreqtime->SetName("vp-freqtime-all");
  gfreqtime->SetMarkerStyle(4);
  gfreqtime->SetMarkerColor(1);
  gfreqtime->SetMarkerSize(0.8);
  delete hsnrtime;
  delete gsnrtime;
  gsnrtime = TP->GetTGraph(&hsnrtime, "snrtime", TMath::Min((UInt_t)TMath::Max(summary_snrthr, 0), (UInt_t)snrthr.size()-1));
  hsnrtime->SetName("vp-snrtime-all-frame");
  gsnrtime->SetName("vp-snrtime-all");
  gsnrtime->SetMarkerStyle(4);
  gsnrtime->SetMarkerColor(1);
  gsnrtime->SetMarkerSize(0.8);
  delete hsnrfreq;
  delete gsnrfreq;
  gsnrfreq = TP->GetTGraph(&hsnrfreq, "snrfreq", TMath::Min((UInt_t)TMath::Max(summary_snrthr, 0), (UInt_t)snrthr.size()-1));
  hsnrfreq->SetName("vp-snrfreq-all-frame");
  gsnrfreq->SetName("vp-snrfreq-all");
  gsnrfreq->SetMarkerStyle(4);
  gsnrfreq->SetMarkerColor(1);
  gsnrfreq->SetMarkerSize(0.8);

  return;
}

////////////////////////////////////////////////////////////////////////////////////
bool VetoPerf::MakeVetoPerformance(const unsigned int aVetoIndex){
////////////////////////////////////////////////////////////////////////////////////
  if(TP==NULL){
    cerr<<"VetoPerf::MakeVetoPerformance: the triggers must be defined first."<<endl;
    return false;
  }
  if(aVetoIndex>=veto.size()){
    cerr<<"VetoPerf::MakeVetoPerformance: the veto index "<<aVetoIndex<<" does not exist"<<endl;
    return false;
  }
  
  // get veto segments
  Segments *vetoseg = veto[aVetoIndex]->GetSegments(InSeg);
  
  // set veto numbers
  veto[aVetoIndex]->SetVetoNumbers(vetoseg);

  // open txt file for vetoed triggers
  gwl_ss<<outdir<<"/veto"<<aVetoIndex<<"-clusters.txt";
  ofstream ofs_triggers;
  if(printvetoclusters){
    ofs_triggers.open(gwl_ss.str().c_str(), ofstream::out);
    ofs_triggers.flags(ios::fixed);
    ofs_triggers.precision(3);
  }
  gwl_ss.clear(); gwl_ss.str("");

  // apply veto to trigger set
  double ct, cf, csnr;
  unsigned int segn;
  for(unsigned int c=0; c<TP->GetClusterN(); c++){

    // the cluster is excluded
    if(TP->GetClusterTag(c)<0) continue;
    
    // cluster peak time and SNR
    ct=TP->GetClusterTime(c);
    cf=TP->GetClusterFrequency(c);
    csnr=TP->GetClusterSnr(c);

    // activate cluster (1 = not vetoed)
    TP->SetClusterTag(c,1);

    // the cluster is vetoed
    if(vetoseg->IsInsideSegment(segn, ct)){

      // tag cluster (=0)
      TP->SetClusterTag(c,0);

      // write cluster
      if(ofs_triggers.is_open()) ofs_triggers<<ct<<" "<<cf<<" "<<csnr<<endl;

      // save cluster in TGraph
      if((summary_snrthr>=0) && (csnr>=snrthr[summary_snrthr])) veto[aVetoIndex]->VetoClusterTimeFrequencySnr(ct, cf, csnr);
      
      // veto perf for SNR categories
      for(unsigned int s=0; s<snrthr.size(); s++){

        if(csnr>=snrthr[s]){

          // one more vetoed cluster
          veto[aVetoIndex]->VetoCluster(s);

          // was the segment already used for that category?
	  if(vetoseg->GetTag(segn)<(int)s){
            veto[aVetoIndex]->VetoSegment(s);
            vetoseg->SetTag(segn,s); // tag segment as used
          }
        }
      }
    }
  }

  if(ofs_triggers.is_open()) ofs_triggers.close();
                          
  // remove vetoed clusters
  for(unsigned int s=0; s<snrthr.size(); s++)
    TP->GetCollectionSelection(s)->UseClusterTag(1);

  // make collections
  TP->MakeCollection(-1, InSeg);
  
  // print
  cout<<"veto: "<<veto[aVetoIndex]->GetName()<<endl;
  cout<<"\tdead time: "<<veto[aVetoIndex]->GetDeadTime()<<" s"<<endl;
  cout<<"\tnumber of active veto segments: "<<veto[aVetoIndex]->GetVetoSegmentN()<<endl;
  cout<<"\tnumber of vetoed clusters: ";
  for(unsigned int s=0; s<snrthr.size(); s++) cout<<snrthr[s]<<"/"<<veto[aVetoIndex]->GetVetoClusterN(s)<<" ";
  cout<<endl;
 
  // dump veto segments
  if(printveto && vetoseg->GetN()) {
    gwl_ss<<outdir<<"/veto"<<aVetoIndex<<".txt";
    vetoseg->Dump(2, gwl_ss.str());
    gwl_ss.clear(); gwl_ss.str("");
  }
  delete vetoseg;

  // print 2D plots
  if(veto[aVetoIndex]->GetVetoClusterN(0) > 0){
    gwl_ss<<TP->GetNameConv()<<"-veto"<<aVetoIndex;
    TP->PrintPlot("freqtime");
    TP->DrawLegend();
    TP->Print(outdir+"/"+gwl_ss.str()+"_freqtime.png");
    TP->PrintPlot("snrtime");
    TP->DrawLegend();
    TP->Print(outdir+"/"+gwl_ss.str()+"_snrtime.png");
    TP->PrintPlot("snrfreq");
    TP->DrawLegend();
    TP->Print(outdir+"/"+gwl_ss.str()+"_snrfreq.png"); 
    TP->UnDrawLegend();
    TP->UnDrawLegend();
    TP->UnDrawLegend();

    // print snr plot
    TH1D *hsnr_veto = TP->GetTH1D("snr", 0);
    hsnr_veto->SetFillColor(hsnr_veto->GetLineColor());
    hsnr_veto->SetFillStyle(3001);
    TP->Draw(hsnr);
    TP->Draw(hsnr_veto, "same");
    TP->Print(outdir+"/"+gwl_ss.str()+"_snr.png");
    gwl_ss.clear(); gwl_ss.str("");
    delete hsnr_veto;
  }
  
  return true;
}
   
////////////////////////////////////////////////////////////////////////////////////
void VetoPerf::MakeCollections(void){
////////////////////////////////////////////////////////////////////////////////////

  // SNR max
  double snrmax;
  if(TP->GetCollectionSelection(0)->GetSnrMax()<50.0)         snrmax = 50.0;
  else if(TP->GetCollectionSelection(0)->GetSnrMax()<100.0)   snrmax = 100.0;
  else if(TP->GetCollectionSelection(0)->GetSnrMax()<1000.0)  snrmax = 1000.0;
  else if(TP->GetCollectionSelection(0)->GetSnrMax()<10000.0) snrmax = 10000.0;
  else                                                        snrmax = 100000.0;
  if(snrmax<=snrthr.back()) snrmax = 2*snrthr.back();
  
  // time binning
  double gps_start = 0.0;
  double gps_end = 1.0;
  if(InSeg->GetLiveTime()>0){
    gps_start = InSeg->GetFirst();
    gps_end = InSeg->GetLast();
  }

  // Apply plot selections
  for(unsigned int s=0; s<snrthr.size(); s++){

    // time format
    TP->GetCollectionSelection(s)->UseDateTime();

    // snr thresholds
    TP->GetCollectionSelection(s)->SetSnrRange(snrthr[s], snrmax);
    gwl_ss<<"SNR \\ge "<<fixed<<setprecision(1)<<snrthr[s];
    TP->SetCollectionLegend(s, gwl_ss.str());
    gwl_ss.str(""); gwl_ss.clear();

    // time selection
    TP->GetCollectionSelection(s)->SetTimeRange(gps_start, gps_end);

    // use clusters
    TP->GetCollectionSelection(s)->UseClusterTag(1);

    // set collection marker
    if(s) TP->SetCollectionMarker(s, 20, TMath::Max(0.3,(double)(s+1)/(double)(snrthr.size())));
    else TP->SetCollectionMarker(s, 1, 1);
		       
    // set collection color
    TP->SetCollectionColor(s,TP->GetColorPalette((int)((double)(s+1)/(double)(snrthr.size())*(double)(TP->GetNumberOfColors()))-2));
      
  }

  // make collections
  TP->MakeCollection(-1, InSeg);

  return;
}
 
