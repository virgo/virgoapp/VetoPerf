/**
 * @file 
 * @brief VetoPerf main class.
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#ifndef __VVetoPerf__
#define __VVetoPerf__

#include <TriggerPlot.h>
#include <ffl.h>
#include "VPconfig.h"

using namespace std;

/**
 * @brief Veto class.
 * @details This class saves the veto definition and the performance numbers.
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
class Veto{

public:
  
  /**
   * @name Constructors and destructors
   @{
  */
  /**
   * @brief Constructor of the Veto class.
   * @details The Veto structure is associated to either a veto channel or a veto file.
   *
   * For a veto channel, the veto is active when the channel value is above a threshold defined with SetChannelThreshold() (default = 0.5). The veto channel is found in frame files managed by a ffl object given in this constructor. The veto definition is simply the veto channel name.
   *
   * A veto file is a text file with a list of time segments (two columns: GPS start - GPS end) in which the veto is active. If the ffl pointer is set to NULL, the veto definition is interpreted as the path to the veto file.
   * @param[in] aFfl Pointer to a ffl to retrieve the veto channel. Use a pointer to NULL for a veto file.
   * @param[in] aDefinition Veto definition: channel name if veto channel, file path if veto file.
   * @param[in] aSnrN Number of SNR categories.
   */
  Veto(ffl* aFfl, const string aDefinition, const unsigned int aSnrN);

  /**
   * @brief Constructor of the Veto class.
   * @details The Veto structure is associated to a Segments object listing the segments when the veto is active.
   * @param[in] aName Veto name.
   * @param[in] aSegments List of veto segments.
   * @param[in] aSnrN Number of SNR categories.
   */
  Veto(const string aName, Segments* aSegments, const unsigned int aSnrN);

  /**
   * @brief Destructor of the Veto class.
   */
  virtual ~Veto(void);
  /**
     @}
  */

  /**
   * @brief Defines the channel threshold to define the veto.
   * @details The veto is active if the channel value is above this threshold.
   * @param[in] aThreshold Threshold value.
   */
  inline void SetChannelThreshold(const double aThreshold){ vetothr = aThreshold; };

  /**
   * @brief Sets the veto color.
   * @param[in] aColor ROOT color code.
   */
  inline void SetColor(Color_t aColor){
    gfreqtime->SetMarkerColor(aColor);
    gsnrtime->SetMarkerColor(aColor);
    gsnrfreq->SetMarkerColor(aColor);
  };

  /**
   * @brief Returns the marker color for this veto.
   */
  inline Color_t GetColor(void){
    return gfreqtime->GetMarkerColor();
  };

  /**
   * @brief Sets veto numbers: dead-time and number of active segments.
   * @param[in] aVetoSegments List of veto segments.
   */
  inline void SetVetoNumbers(Segments *aVetoSegments){
    vetodt = aVetoSegments->GetLiveTime();
    n_seg = aVetoSegments->GetN();
  };

  /**
   * @brief Increments the number of vetoed cluster for a given SNR category.
   * @param[in] aSnrIndex SNR category index.
   * @pre The SNR index must be valid.
   */
  inline void VetoCluster(const unsigned int aSnrIndex){
    n_cveto[aSnrIndex]++;
  };

  /**
   * @brief Saves the time, frequency, and SNR of a vetoed cluster.
   * @details Use this function to fill a TGraph with veto clusters. The resulting TGraphs can be obtained with GetVetoTimeFrequency(), GetVetoTimeSnr(), and GetVetofrequencySnr().
   * @param[in] aTime Vetoed cluster GPS time [s].
   * @param[in] aFrequency Vetoed cluster frequency [Hz].
   * @param[in] aSnr Vetoed cluster SNR.
   */
  inline void VetoClusterTimeFrequencySnr(const double aTime, const double aFrequency, const double aSnr){
    gfreqtime->SetPoint(gfreqtime->GetN(), aTime, aFrequency);
    gsnrtime->SetPoint(gsnrtime->GetN(), aTime, aSnr);
    gsnrfreq->SetPoint(gsnrfreq->GetN(), aFrequency, aSnr);
  };

  /**
   * @brief Increments the number of used segments for a given SNR category.
   * @param[in] aSnrIndex SNR category index.
   * @pre The SNR index must be valid.
   */
  inline void VetoSegment(const unsigned int aSnrIndex){
    n_seguse[aSnrIndex]++;
  };

  /**
   * @brief Returns the list of veto time segments.
   * @details If the veto is defined with a veto file, the intersection of the veto segments in the file with the input list of segments.
   *
   * For a veto channel, the veto is active if the channel value is above the threshold defined with SetChannelThreshold(). The intersection between the input segments and the segments when the veto is active is returned.
   *
   * For a Segments object, a copy is made and it is intersected with the input list of segments.
   * @warning All segment are tagged to -1
   * @param[in] aInSegments List of input segments.
   * @pre The list of input segments is assumed to be valid.
   */
  Segments* GetSegments(Segments *aInSegments);

  /**
   * @brief Returns the veto name.
   */
  inline string GetName(void){
    return vetoname;
  }

  /**
   * @brief Returns the veto dead time [s].
   */
  inline double GetDeadTime(void){
    return vetodt;
  }

  /**
   * @brief Returns the number of active veto segments.
   */
  inline unsigned int GetVetoSegmentN(void){
    return n_seg;
  }

  /**
   * @brief Returns the number of vetoed clusters.
   * @param[in] aSnrIndex SNR category index.
   * @pre The SNR index must be valid.
   */
  inline unsigned int GetVetoClusterN(const unsigned int aSnrIndex){
    return n_cveto[aSnrIndex];
  }

  /**
   * @brief Returns the number of used veto segments.
   * @param[in] aSnrIndex SNR category index.
   * @pre The SNR index must be valid.
   */
  inline unsigned int GetVetoUseSegmentN(const unsigned int aSnrIndex){
    return n_seguse[aSnrIndex];
  }

  /**
   * @brief Returns the pointer to the time-frequency distribution of vetoed clusters.
   * @details The TGraph must have been previously filled with VetoClusterTimeFrequencySnr().
   */
  inline TGraph* GetVetoFreqTime(void){
    return gfreqtime;
  }

  /**
   * @brief Returns the pointer to the time-SNR distribution of vetoed clusters.
   * @details The TGraph must have been previously filled with VetoClusterTimeFrequencySnr().
   */
  inline TGraph* GetVetoSnrTime(void){
    return gsnrtime;
  }

  /**
   * @brief Returns the pointer to the frequency-SNR distribution of vetoed clusters.
   * @details The TGraph must have been previously filled with VetoClusterTimeFrequencySnr().
   */
  inline TGraph* GetVetoSnrFreq(void){
    return gsnrfreq;
  }

 private:

  ffl *vetoffl;           ///< Pointer to ffl for veto channel. Use NULL for a veto file.
  Segments *vetoseg;      ///< Pointer to a Segments object. NULL is irrelevant.
  string vetofile;        ///< Path to veto file.
  string vetoname;        ///< Veto name.
  double vetothr;         ///< Channel threshold to activate the veto.
  string vetodir;         ///< Veto directory.
  double vetodt;          ///< Veto dead time [s].
  unsigned int n_seg;     ///< Number of veto active segments
  unsigned int n_snr;     ///< Number cluster SNR categories.
  unsigned int *n_seguse; ///< Number of used veto segments.
  unsigned int *n_cveto;  ///< Number of vetoed clusters.
  TGraph *gfreqtime;      ///< Frequency-time distribution for vetoed clusters.
  TGraph *gsnrtime;       ///< SNR-time distribution for vetoed clusters.
  TGraph *gsnrfreq;       ///< SNR-frequency distribution for vetoed clusters.

};

/**
 * @brief Veto performance class.
 * @details This class is designed to evaluate the performance of time-domain vetoes against a set of triggers.
 *
 * The set of triggers is defined with SetTriggers(). The triggers must be clustered first as clusters are used to test the vetoes. The vetoes can be provided as text files (AddVetoFile()) or as ADC channels (AddVetoChannel) found in frame files.
 *
 * The performance algorithm is performed with MakeVetoPerformance() called for each veto.
 *
 * An html report can be generated with MakeReport().
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
class VetoPerf{

 public:

  /**
   * @name Constructors and destructors
   @{
  */
  /**
   * @brief Constructor of the VetoPerf class.
   * @details A list of SNR thresholds must be provided to define the categories of triggers to evaluate the veto performance. 
   * @param[in] aSnrThr List of SNR thresholds. Empty vector = use default values: 5, 8, 10, and 20.
   */
  VetoPerf(const vector <double> aSnrThr);

  /**
   * @brief Destructor of the VetoPerf class.
   */
  virtual ~VetoPerf(void);
  /**
     @}
  */

  /**
   * @brief Returns the number of SNR thresholds.
   */
  inline unsigned int GetSnrThrN(void){ return snrthr.size(); };

  /**
   * @brief Returns the SNR threshold for a given SNR category.
   * @param[in] aSnrIndex SNR category index.
   * @pre The SNR index must be valid!
   */
  inline double GetSnrThr(const unsigned int aSnrIndex){ return snrthr[aSnrIndex]; };

  /**
   * @brief Returns the number of active clusters for a given SNR category.
   * @param[in] aSnrIndex SNR category index.
   * @pre The SNR index must be valid!
   */
  inline unsigned int GetClusterN(const unsigned int aSnrIndex){ return n_c_snr[aSnrIndex]; };

  /**
   * @brief Sets a ffl pointer to define veto channels.
   * @details Provide the ffl object used to extract veto channels.
   * @param[in] aFfl Pointer to a valid ffl object. Use a pointer to NULL if no veto channels.
   */
  inline void SetFfl(ffl *aFfl=NULL){ ffldata=aFfl; };

  /**
   * @brief Sets the path to the output directory.
   * @warning The directory must exist.
   * @param[in] aOutdir Path to output directory.
   */
  inline void SetOutputDirectory(const string aOutdir){
    if(filesystem::is_directory(aOutdir)) outdir=aOutdir;
  };
  
  /**
   * @brief Prints a summary plot showing the vetoed cluster distribution.
   * @details It is possible to print a summary plot to show the impact of the 10 most efficient vetoes. The summary plot is a time-frequency distribution of the clusters where each cluster is marked when it is vetoed.
   *
   * The SNR index must be given to select which SNR threshold (defined in the constructor) is applied.
   * @warning The summary plot is only produced when calling MakeReport().
   * @param[in] aSnrIndex SNR category index. Use -1 to not print the summary plot.
   */
  inline void PrintVetoSummary(const int aSnrIndex){
    summary_snrthr = TMath::Min(aSnrIndex, (int)(snrthr.size()-1));
  };
  
  /**
   * @brief Sets a flag to print vetoes in a TXT file.
   * @param[in] aFlag Set this flag to true to print vetoes in a file.
   */
  inline void PrintVeto(const bool aFlag){
    printveto = aFlag;
  };
  
  /**
   * @brief Sets a flag to print vetoed clusters in a TXT file.
   * @param[in] aFlag Set this flag to true to print clusters in a file.
   */
  inline void PrintVetoClusters(const bool aFlag){
    printvetoclusters = aFlag;
  };
  
  /**
   * @brief Adds a subtitle in the html report.
   * @param[in] aSubTitle Subtitle string.
   */ 
  inline void SetSubTitle(const string aSubTitle){ subtitle=aSubTitle; };

  /**
   * @brief Returns the number of vetoes.
   */
  inline unsigned int GetVetoN(void){ return veto.size(); };

  /**
   * @brief Adds a veto to the veto list.
   * @returns The current number of vetoes.
   * @param[in] aVetoName Veto name.
   * @param[in] aVetoSeg Pointer to the list of segments when the veto is active. DO NOT DELETE.
   */
  inline unsigned int AddVeto(const string aVetoName, Segments *aVetoSeg){
    veto.push_back(new Veto(aVetoName, aVetoSeg, snrthr.size()));
    return veto.size();
  };
  
  /**
   * @brief Adds a veto file to the veto list.
   * @returns The current number of vetoes.
   * @param[in] aVetoFile Path to the veto file.
   */
  inline unsigned int AddVetoFile(const string aVetoFile){
    veto.push_back(new Veto(NULL, aVetoFile, snrthr.size()));
    return veto.size();
  };
  
  /**
   * @brief Adds a veto channel to the veto list.
   * @returns The current number of vetoes.
   * @warning The FFL file must be defined first with SetFfl() as the FFL file is associated to the veto definition.
   * @param[in] aVetoChannelName Veto channel name.
   * @param[in] aThreshold Channel threshold to define the veto.
   */
  inline unsigned int AddVetoChannel(const string aVetoChannelName, const double aThreshold){
    veto.push_back(new Veto(ffldata, aVetoChannelName, snrthr.size()));
    veto.back()->SetChannelThreshold(aThreshold);
    return veto.size();
  };
  
  /**
   * @brief Sets triggers and generates trigger plots.
   * @details This function sets the triggers to evaluate the veto performance.
   * The cluster distribution plots are saved in the output directory defined with SetOutputDirectory().
   *
   * The input TriggerPlot structure must be defined such that the number of collections matches the number of SNR thresholds defined in the constructor.
   * Triggers must be clustered before calling this function.
   * Clusters with a negative tag are ignored.
   *
   * By default, the active time segments are given by the input trigger structure. Optionally, a list of time segments can be given to select clusters in time.
   *
   * @param[in] aTriggers TriggerPlot structure.
   * @param[in] aInSeg Input segments for trigger selection. Set this to NULL to consider all the triggers.
  */
  void SetTriggers(TriggerPlot* aTriggers, Segments *aInSeg=NULL);

  /**
   * @brief Run the veto performance algorithm.
   * @details The list of veto segments is extracted. It is used to veto clusters set with SetTriggers(). The veto performance is measured. Trigger plots after the veto is applied are saved in the ouput directory.
   * @param[in] aVetoIndex Veto index. Must exist!
  */
  bool MakeVetoPerformance(const unsigned int aVetoIndex);

  /**
   * Make html report.
  */
  bool MakeReport(void);

 private:

  // GENERAL
  time_t timer_start;          ///< Timer start.
  string subtitle;             ///< Subtitle.
  string outdir;               ///< Output directory.
  Segments *InSeg;             ///< Input segments.

  // VETOES
  ffl *ffldata;                ///< Pointer to a ffl object.
  vector <Veto*> veto;         ///< Veto objects.
  bool printveto;              ///< Flag to print vetoes in a TXT file.
  int summary_snrthr;          ///< SNR index for the summary plot.

  // TRIGGERS
  TriggerPlot *TP;             ///< Pointer to triggers.
  unsigned int *n_c_snr;       ///< Number of active clusters in SNR categories.
  vector <double> snrthr;      ///< List of SNR thresholds.
  TH1D *hsnr;                  ///< SNR distribution.
  TH1D *hfreqtime;             ///< Frequency-time distribution (frame).
  TGraph *gfreqtime;           ///< Frequency-time distribution.
  TH1D *hsnrtime;              ///< SNR-time distribution (frame).
  TGraph *gsnrtime;            ///< SNR-time distribution.
  TH1D *hsnrfreq;              ///< SNR-frequency distribution (frame).
  TGraph *gsnrfreq;            ///< SNR-frequency distribution.
  bool printvetoclusters;      ///< Flag to print vetoed clusters in a TXT file.
  
  /**
   * @brief Parameterizes the TriggerPlot collections.
   */ 
  void MakeCollections(void);

  /**
   * @brief Sorts vetoes by decreasing efficiencies (for html report).
   */
  vector<unsigned int> SortVetoes(void);

  ClassDef(VetoPerf,0)  
};


#endif


