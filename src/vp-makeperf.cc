/**
 * @file 
 * @brief Program to evaluate the performance of time-domain vetoes against a list of triggers.
 * @details The program takes as a basic input a set of triggers and a set of vetoes. It generates a html report to present the result of the analysis. `vp-makeperf` uses methods of the VetoPerf class.
 *
 * ## Input
 *
 * The triggers must be provided as ROOT files following the GWOLLUM convention. The triggers are clustered: clusters are used to evaluate the veto performance. The `vp-makeperf` program is able to discover centralized Omicron triggers using the `channel=` option.
 *
 * The performance is evaluated for sub-categories of clusters defined by a SNR threshold: `snr-thresholds=`.
 *
 * A veto is defined by a list of time segments. This list can be provided with a text file listing the GPS time segments (`veto-files=`). It is also possible to provide a veto channel and a threshold: the veto time segments are extracted when the channel value is above the threshold (`veto-channels=`). For veto channels, a FFL file must be provided with `ffl=`.
 *
 * ## Basic principle
 * 
 * A cluster is vetoed if the cluster peak time stands inside a veto time segment. The veto performance is characterized by a few absolute numbers:
 * - dead-time: total amount of time rejected by a veto
 * - number of vetoed clusters as function of the SNR
 * - number of veto segments actually used to veto clusters 
 *
 * It is also convenient to use relative numbers:
 * - dead-time over the total livetime
 * - number of vetoed clusters over the total number of clusters in the input data set. This is also called "veto efficiency". This is also given as a function of the SNR.
 * - number of veto segments actually used to veto clusters over the total number of veto segments. This is also called "used percentage".
 *
 * @warning These relative numbers only make sense if they are computed over the same reference livetime. In other words, it is important that both the veto segments and the clusters are produced over the same time segments (no more, no less). If this is not the case, the intersection must be considered (`vp-makeperf` will do it for you).
 *
 * `vp-makeperf` measures all the numbers presented above. It also generates plots with cluster distributions, before and after applying the vetoes.
 *
 * ## Running `vp-makeperf`
 *
 * If you work with veto segments in text files, use the following command line to run `vp-makeperf`:
 * @verbatim
 vp-makeperf gps-start=[GPS start] gps-end=[GPS stop] channel=[trigger channel name] veto-files=[list of veto files]
 @endverbatim
 * where `[GPS start]` and `[GPS stop]` are the GPS times where to start and stop the analysis. `[trigger channel name]` is the name of the channel (e.g. `V1:Hrec_hoft_16384Hz`) you want to consider for your trigger data set (Omicron centralized triggers). Finally, you must provide a list of vetoes as a list of text files separated by a `;`: `"/path/to/my/DQ/flags/veto1.txt;/path/to/my/DQ/flags/veto2.txt;/path/to/my/DQ/flags/veto3.txt"`. The input veto file is a text file with 2 columns: `[gps start]` `[gps end]`.
 *
 * If you work with veto channels, use the following command line:
 * @verbatim
 vp-makeperf gps-start=[GPS start] gps-end=[GPS stop] channel=[trigger channel name] veto-channels=[list of channels] ffl=[path to the ffl]
 @endverbatim
 * where `[list of channels]` is the list of channels and thresholds: `"V1:DQ1&0.5;V1:DQ2&1.0e-21"`. The veto is active when the the channel (here: V1:DQ1 and V1:DQ2) take values above the threshold (here: 0.5 and 1.0e-21). The path to the ffl file must be given with the `ffl=` option.
 *
 * Of course, the `vp-makeperf` tool can be used with both veto files and veto channels.
 *
 * As explained below in the Options section, it is also possible to choose a set of segments to run the analysis.
 *
 * @verbatim
 vp-makeperf segment-file=[segment file] (...)
 @endverbatim
 * In this way, one can really control the input of the code, for instance by running on selected 'SCIENCE after CAT1' segments whereas Omicron triggers are produced more loosely -- usually when the detector is in LOW_NOISE configuration.
 *
 * ## Options
 *
 * `vp-makeperf` comes with many extra options which can be used at the command line. Just type `vp-makeperf` to get the list of options:
 * - `channel=X`: Specify the name of the channel processed with Omicron. This defines your trigger data set to evaluate the veto performance. This option only works if Omicron triggers are centrally managed.
 * - `file=X`: Another way to provide triggers is to give a list of ROOT files (GWOLLUM convention). This list can be given as a list of files separated by a space or by a file pattern like `/path/to/my/files/\*.root`. You can even give a list of file patterns.
 * - `gps-start=X` and `gps-end=Y`: This option defines the time range of your analysis: use GPS seconds.
 * - `segment-file=X`: Alternatively, you can give a list of time segments to perform the vetoperf analysis. This is provided as a text file with 2 columns: [gps start] and [gps end].
 * - `veto-files="X;Y;Z"`: Specify a list of veto files of which you want to evaluate the performance. A veto file is a text file defining when the veto is active, with 2 columns: [gps start] and [gps end].
 * - `veto-channels="X\&tx;Y\&ty`: A veto can also be defined from an ADC channel in frame files. Here, you define your veto as a channel name (`X`) and a threshold (`tx`). The veto is active when channel `X` takes vales above `tx`. This option only works if a FFL file is given with `ffl=X`.
 * - `ffl=X`: Path to the FFL file where to find the veto channels.
 * - `outdir=X`: Path to the output directory where the html report and plots are dumped (must exist!).
 * - `snr-thresholds="X;Y;Z"`: List of SNR thresholds to define categories of triggers. Veto performance numbers are computed for each categories.
 * - `freq-min=X` and `freq-max=Y`: This option defines a frequency range [Hz] to select the triggers.
 * - `print-veto-summary=X`: Use this option to print a veto summary plot in the html report. You must specify the index of the SNR category you want to use. For example, if `snr-thresholds="X;Y;Z"` and `print-veto-summary=1`, then only triggers with `SNR > Y` are used in the veto summary plot.
 * - `print-veto=1/0`: Use 1 to link the list of veto segments in the html report. Use 0 otherwise.
 * - `print-veto-clusters=1/0`: Use 1 to link the list of vetoed triggers in the html report. Use 0 otherwise.
 * - `cluster-dt=X`: Input triggers are clustered before evaluating the veto performance. Here you can adjust the clustering time window [s].
 * 
 * @snippet this vp-makeperf-usage 
 *
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "VVetoPerf.h"
#include <OmicronUtils.h>

/**
 * @brief Usage funcion.
 */
int printhelp(void){
  cerr<<endl;
  //! [vp-makeperf-usage]
  cerr<<"Usage:"<<endl; 
  cerr<<endl;
  cerr<<"vp-makeperf channel=[channel name] \\"<<endl;
  cerr<<"            file=[trigger file pattern] \\"<<endl;
  cerr<<"            gps-start=[GPS start] \\"<<endl;
  cerr<<"            gps-end=[GPS end] \\"<<endl;
  cerr<<"            veto-files=[list of veto files] \\"<<endl;
  cerr<<"            veto-channels=[list of veto channels] \\"<<endl;
  cerr<<"            ffl=[path to ffl] \\"<<endl;
  cerr<<"            segment-file=[input segment file] \\"<<endl;
  cerr<<"            outdir=[output directory] \\"<<endl; 
  cerr<<"            snr-thresholds=[list of SNR thresholds] \\"<<endl; 
  cerr<<"            freq-min=[minimum frequency] \\"<<endl; 
  cerr<<"            freq-max=[maximum frequency] \\"<<endl;
  cerr<<"            print-veto-summary=[print veto summary plot] \\"<<endl;
  cerr<<"            print-veto=[1/0] \\"<<endl;
  cerr<<"            print-veto-clusters=[1/0] \\"<<endl;
  cerr<<"            cluster-dt=[cluster time window]"<<endl;
  cerr<<endl;
  cerr<<endl;
  cerr<<"[channel name]             channel name used to retrieve centralized Omicron triggers"<<endl;
  cerr<<"[trigger file pattern]     file pattern to ROOT trigger files (GWOLLUM convention)"<<endl;
  cerr<<"[GPS start]                starting GPS time (integer only)"<<endl;
  cerr<<"[GPS end]                  stopping GPS time (integer only)"<<endl;
  cerr<<"[list of veto files]       list of veto files separated by a ';'"<<endl;
  cerr<<"[list of veto channels]    list of veto channels&threshold separated by a ';'"<<endl;
  cerr<<"                           each channel must be given a threshold: [name]&[threshold]"<<endl;
  cerr<<"[path to ffl]              path to ffl for veto channels (default: /virgoData/ffl/raw.ffl)"<<endl;
  cerr<<"[input segment file]       path to input segment file (default: none)"<<endl;
  cerr<<"[output directory]         path to output directory (default: current)"<<endl;
  cerr<<"[list of SNR thresholds]   list of SNR thresholds. By default, snr-thresholds=\"5;8;10;20\""<<endl;
  cerr<<"[minimum frequency]        minimum trigger frequency [Hz]. By default, use the minimum trigger frequency."<<endl;
  cerr<<"[maximum frequency]        maximum trigger frequency [Hz]. By default, use the maximum trigger frequency."<<endl;
  cerr<<"[print veto summary]       SNR category index. By default print-veto-summary=-1."<<endl;
  cerr<<"[1/0]                      1=YES, 0=NO. By default NO."<<endl;
  cerr<<"[cluster time window]      cluster time window [s]. By default, = 0.1"<<endl;
  //! [vp-makeperf-usage]
  cerr<<endl;
  cerr<<"See also: https://virgo.docs.ligo.org/virgoapp/VetoPerf/vp-makeperf_8cc.html"<<endl;
  cerr<<endl;
  return 1;
}

/**
 * @brief Main program.
 */
int main (int argc, char* argv[]){


  if(argc>1&&!((string)argv[1]).compare("version")){
    return VP_PrintVersion();
  }

  // check options
  if(argc<2) return printhelp();

  // default arguments
  string chname="";
  string tfilepat="";
  unsigned int gps_start=0;
  unsigned int gps_end=0;
  string veto_files="";
  string veto_channels="";
  string segment_file="";
  string outdir=".";
  string fflpath="/virgoData/ffl/raw.ffl";
  string snrthrs="5;8;10;20";
  double freqmin=-1.0;
  double freqmax=1e20;
  int print_veto_summary=-1;
  bool print_veto=false;
  bool print_veto_clusters=false;
  double cdt=0.1;
  
  // loop over arguments
  vector <string> sarg;
  string scommand="";
  for(int a=0; a<argc; a++){
    scommand+=(string)argv[a]+" ";
    sarg=SplitString((string)argv[a],'=');
    if(sarg.size()!=2) continue;
    if(!sarg[0].compare("channel"))         chname=(string)sarg[1];
    if(!sarg[0].compare("file"))            tfilepat=(string)sarg[1];
    if(!sarg[0].compare("gps-start"))       gps_start=stoul(sarg[1].c_str());
    if(!sarg[0].compare("gps-end"))         gps_end=stoul(sarg[1].c_str());
    if(!sarg[0].compare("veto-files"))      veto_files=(string)sarg[1];
    if(!sarg[0].compare("veto-channels"))   veto_channels=(string)sarg[1];
    if(!sarg[0].compare("ffl"))             fflpath=(string)sarg[1];
    if(!sarg[0].compare("segment-file"))    segment_file=(string)sarg[1];
    if(!sarg[0].compare("outdir"))          outdir=(string)sarg[1];
    if(!sarg[0].compare("snr-thresholds"))  snrthrs=(string)sarg[1];
    if(!sarg[0].compare("freq-min"))        freqmin=atof(sarg[1].c_str());
    if(!sarg[0].compare("freq-max"))        freqmax=atof(sarg[1].c_str());
    if(!sarg[0].compare("print-veto-summary")) print_veto_summary=atoi(sarg[1].c_str());
    if(!sarg[0].compare("print-veto"))      print_veto=!!atoi(sarg[1].c_str());
    if(!sarg[0].compare("print-veto-clusters")) print_veto_clusters=!!atoi(sarg[1].c_str());
    if(!sarg[0].compare("cluster-dt"))      cdt=atof(sarg[1].c_str());
  }

  // input segments
  Segments *inseg = NULL;

  // timing if any
  if(segment_file.compare("")){
    inseg = new Segments(segment_file);
    if(gps_start>0){
      inseg->TruncateAfter(gps_start);
    }
    if(gps_end>0){
      inseg->TruncateBefore(gps_end);
    }
  }
  else if((gps_start!=0) && (gps_end!=0)){
    inseg = new Segments(gps_start, gps_end);
  }
  else{};

  // standard triggers
  if(chname.compare("")){
    if((inseg==NULL) || inseg->GetN()==0){
      cerr<<"A timing must be specified to fetch omicron triggers for "<<chname<<endl;
      cerr<<"Type vp-makeperf for help"<<endl;
      return 1;
    }
    tfilepat = GetOmicronFilePattern(chname, inseg->GetFirst(), inseg->GetLast());
  }

  if(!tfilepat.compare("")){
    cerr<<"Input triggers must be specified either with a list of files or a channel name"<<endl;
    cerr<<"Type vp-makeperf for help"<<endl;
    return 1;
  }

  // snr thresholds
  sarg=SplitString(snrthrs,';');
  vector <double> snrthr;
  for(unsigned int s=0; s<sarg.size(); s++) snrthr.push_back(atof(sarg[s].c_str()));
  if(!snrthr.size()){
    cerr<<"A set of SNR thresholds must be provided."<<endl;
    cerr<<"Type vp-makeperf for help"<<endl;
    return 1; 
  }
  sort(snrthr.begin(), snrthr.end());

  // init VetoPerf object
  VetoPerf *vp = new VetoPerf(snrthr);
  vp->SetOutputDirectory(outdir);
  vp->SetSubTitle(scommand);
  vp->PrintVetoSummary(print_veto_summary);
  vp->PrintVeto(print_veto);
  vp->PrintVetoClusters(print_veto_clusters);

  // input triggers
  TriggerPlot *TP = new TriggerPlot(snrthr.size(), tfilepat, "", "STANDARD", 0);
  if(!TP->GetStatus()) return 2;

  // clustering
  TP->SetClusterizeDt(cdt);
  TP->SetClusterizeSnrThr(snrthr[0]);
  if(!TP->Clusterize(1)) return 3;

  // apply frequency selection
  for(unsigned int c=0; c<TP->GetClusterN(); c++){
    if(TP->GetClusterFrequency(c)<freqmin) TP->SetClusterTag(c,-1);
    if(TP->GetClusterFrequency(c)>freqmax) TP->SetClusterTag(c,-1);
  }
  
  // input ffl
  ffl *fdata = NULL;
  if(filesystem::is_regular_file(fflpath)){
    fdata = new ffl(fflpath, "STANDARD", 0, ".", 1, 0);
    if(!fdata->ExtractChannels(gps_end)) return 4;
  }
  
  // set ffl
  vp->SetFfl(fdata);
  
  // set triggers (inseg can be NULL)
  vp->SetTriggers(TP, inseg);

  // set vetoes
  sarg=SplitString(veto_files,';');
  for(unsigned int s=0; s<sarg.size(); s++) vp->AddVetoFile(sarg[s]);
  sarg=SplitString(veto_channels,';');
  vector<string> chanthr;
  for(unsigned int s=0; s<sarg.size(); s++){
    chanthr=SplitString(sarg[s],'&');
    if(chanthr.size()==2) vp->AddVetoChannel(chanthr[0], atof(chanthr[1].c_str()));
    else vp->AddVetoChannel(chanthr[0], 0.5);
  }
  if(vp->GetVetoN()==0){
    cerr<<"no vetoes"<<endl;
    return 5;
  }

  // measure performance
  for(unsigned int v=0; v<vp->GetVetoN(); v++){
    if(!vp->MakeVetoPerformance(v)) continue;
  }

  // dump html report
  if(!vp->MakeReport()){
    cerr<<"html report failed"<<endl;
    return 6;
  }
  
  delete vp;
  delete TP;
  delete fdata;
  delete inseg;
  return 0;
}
