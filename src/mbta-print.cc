//////////////////////////////////////////////////////////////////////////////
//  Author : florent robinet (LAL - Orsay): robinet@lal.in2p3.fr
//////////////////////////////////////////////////////////////////////////////
#include <ffl.h>
#include <MakeTriggers.h>
#include "VPconfig.h"

using namespace std;

void PrintUsage(void){
  cerr<<endl;
  cerr<<"Usage:"<<endl;
  cerr<<endl;
  cerr<<"print-mbta.exe gps-start=[GPS start] \\"<<endl;
  cerr<<"               gps-end=[GPS end] \\"<<endl;
  cerr<<"               segment=[ffl;channel for analysis segments] \\"<<endl;
  cerr<<"               trigger=[ffl;channel for time;channel for snr:channel for reweighted snr] \\"<<endl;
  cerr<<"               outdir=[path to output directory] \\"<<endl;
  cerr<<"               stream=[stream name] \\"<<endl;
  cerr<<"               time-shift=[time shift in sec]"<<endl;
  cerr<<endl;
  return;
}

int main (int argc, char* argv[]){

  if(argc>1&&!((string)argv[1]).compare("version")){
    PrintVersion();
    return 0;
  }

  // number of arguments
  if(argc<2){
    PrintUsage();
    return 1;
  }

  // list of parameters + default
  const int GPSROOT      = 10000;
  int gps_start          =-1;                  // GPS start
  int gps_end            =-1;                  // GPS end
  string ffltrig;                              // path to ffl (trigger)
  string fflseg;                               // path to ffl (segments)
  string chtime="";                            // channel for trigger time
  string chsnr="";                             // channel for trigger snr
  string chrwsnr="";                           // channel for trigger rw snr
  string chseg="";                             // channel for analysis segments
  string outdir          =".";                 // output directory
  string streamname      ="V1:TRIGGER_STREAM"; // stream name
  double toffset         = 0.0;                // time offset


  // loop over arguments
  vector <string> sarg;
  for(int a=1; a<argc; a++){
    sarg=SplitString((string)argv[a],'=');
    if(sarg.size()!=2) continue;
    if(!sarg[0].compare("gps-start"))    gps_start=atoi(sarg[1].c_str());
    if(!sarg[0].compare("gps-end"))      gps_end=atoi(sarg[1].c_str());
    if(!sarg[0].compare("trigger"))      chtime=(string)sarg[1];
    if(!sarg[0].compare("segment"))      chseg=(string)sarg[1];
    if(!sarg[0].compare("outdir"))       outdir=(string)sarg[1];
    if(!sarg[0].compare("stream"))       streamname=(string)sarg[1];
    if(!sarg[0].compare("time-shift"))   toffset=atof(sarg[1].c_str());
  }

  // check GPS
  if(gps_start<=0||gps_end<=gps_start){
    cerr<<"A valid gps time interval must be provided"<<endl;
    return 1;
  }

  // break input data string
  vector <string> vs = SplitString(chtime, ';');
  if(vs.size()!=4){
    cerr<<"Trigger input: a FFL file and 3 channel names must be provided"<<endl;
    return 1;
  }
  ffltrig=vs[0];
  chtime=vs[1];
  chsnr=vs[2];
  chrwsnr=vs[3];
  vs = SplitString(chseg, ';');
  if(vs.size()!=2){
    cerr<<"Segment input: a FFL file and a channel name must be provided"<<endl;
    return 1;
  }
  fflseg=vs[0];
  chseg=vs[1];
  
  // check input data
  if(!IsTextFile(ffltrig)||!IsTextFile(fflseg)){
    cerr<<"Input data must be provided"<<endl;
    return 1;
  }
  if(!chtime.compare("")){
    cerr<<"An input channel for the trigger time must be provided"<<endl;
    return 1;
  }
  if(!chsnr.compare("")){
    cerr<<"An input channel for the trigger snr must be provided"<<endl;
    return 1;
  }
  if(!chrwsnr.compare("")){
    cerr<<"An input channel for the trigger reweighted snr must be provided"<<endl;
    return 1;
  }
  if(!chtime.compare("")){
    cerr<<"An input channel for the trigger segments must be provided"<<endl;
    return 1;
  }

  // init ffl
  ffl *FFLtrig = new ffl(ffltrig);
  if(!FFLtrig->LoadFrameFile(gps_start)){
    cerr<<"Invalid FFL: "<<ffltrig<<endl;
    return 2;
  }
  if(!FFLtrig->IsChannel(chtime)){
    cerr<<"Missing channel "<<chtime<<" at GPS="<<gps_start<<endl;
    return 2;
  }
  if(!FFLtrig->IsChannel(chsnr)){
    cerr<<"Missing channel "<<chsnr<<" at GPS="<<gps_start<<endl;
    return 2;
  }
  if(!FFLtrig->IsChannel(chsnr)){
    cerr<<"Missing channel "<<chrwsnr<<" at GPS="<<gps_start<<endl;
    return 2;
  }
  ffl *FFLseg = new ffl(fflseg);
  if(!FFLseg->LoadFrameFile(gps_start)){
    cerr<<"Invalid FFL: "<<fflseg<<endl;
    return 2;
  }
  if(!FFLseg->IsChannel(chseg)){
    cerr<<"Missing channel "<<chseg<<" at GPS="<<gps_start<<endl;
    return 2;
  }

  // start Triggers stucture
  MakeTriggers *T = new MakeTriggers(streamname,1);
  T->SetMprocessname("MBTA");
  
  int gps_e;
  double *data_t, *data_s, *data_r, *data_a;
  int dsize_t, dsize_s, dsize_r, dsize_a;
  double dt_t, dt_s, st, en, pt;
  Segments *Seg;// ffl data segments
  
  // loop over GPS range 
  for(int gps=gps_start; gps<gps_end; gps+=GPSROOT){

    // GPS stop for this file
    gps_e=TMath::Min(gps+GPSROOT,gps_end);
    cout<<"Converting triggers between "<<gps<<" and "<<gps_e<<"..."<<endl;

    // make list of segments to process
    Seg = new Segments(gps,gps_e);
    Seg->Intersect(FFLseg->GetSegments());
    Seg->Intersect(FFLtrig->GetSegments());

    // sample size
    int dummy;
    dt_t = 1.0/(double)FFLtrig->GetChannelSampling(chtime,dummy);
    dt_s = 1.0/(double)FFLseg->GetChannelSampling(chseg,dummy);

    // loop over ffl segments
    for(int s=0; s<Seg->GetNsegments(); s++){
      
      // get segment data
      data_a = FFLseg->GetData(dsize_a,chseg,Seg->GetStart(s),Seg->GetEnd(s),0.0);
      if(dsize_a<=0){
	cerr<<"Missing data "<<chseg<<" at GPS="<<Seg->GetStart(s)<<"-"<<Seg->GetEnd(s)<<endl;
	return 4;
      }

      // get time data
      data_t = FFLtrig->GetData(dsize_t,chtime,Seg->GetStart(s),Seg->GetEnd(s),0.0);
      if(dsize_t<=0){
	cerr<<"Missing data "<<chtime<<" at GPS="<<Seg->GetStart(s)<<"-"<<Seg->GetEnd(s)<<endl;
	return 4;
      }

      // get snr data
      data_s = FFLtrig->GetData(dsize_s,chsnr,Seg->GetStart(s),Seg->GetEnd(s),0.0);
      if(dsize_s!=dsize_t){
	cerr<<"Missing data "<<chsnr<<" at GPS="<<Seg->GetStart(s)<<"-"<<Seg->GetEnd(s)<<endl;
	return 4;
      }

      // get rw snr data
      data_r = FFLtrig->GetData(dsize_r,chrwsnr,Seg->GetStart(s),Seg->GetEnd(s),0.0);
      if(dsize_r!=dsize_t){
        cerr<<"Missing data "<<chrwsnr<<" at GPS="<<Seg->GetStart(s)<<"-"<<Seg->GetEnd(s)<<endl;
        return 4;
      }

      // loop over the data segments and get analysis segments
      for(int a=0; a<dsize_a; a++){
	if(data_a[a]!=1) continue;
	T->AddSegment(gps+(double)(a)*dt_s+toffset, gps+(double)(a+1)*dt_s+toffset);
      }
      delete data_a;

      // loop over trigger data
      for(int d=0; d<dsize_t; d++){
	
	if(!data_t[d]) continue; // no trigger
	
	// sample time segment
	st=(double)gps+(double)d*dt_t;
	en=(double)gps+(double)(d+1)*dt_t;
	pt=st/2.0+en/2.0;

	// not in segments
	if(!T->IsInsideSegment(toffset+pt)) continue;
	
	// save trigger
	T->AddTrigger(toffset+pt,       // peak time
		      1.0,              // frequency
		      data_s[d],        // SNR
		      data_r[d],        // Q = RW SNR
		      toffset+st,       // tstart
		      toffset+en,       // tend
		      0.1,              // fstart
		      100.0,            // fend
		      1.0,              // amplitude
		      0.0);             // phase
      }

      // clean data vector
      delete data_t;
      delete data_s;
      delete data_r;
    }

    // add segment
    delete Seg;

    // save trigger file
    T->SortTriggers();
    T->Write(outdir,"root");
  }


  // cleaning
  delete T;
  delete FFLtrig;
  delete FFLseg;

  return 0;
}

// MbtaV_R1_00_dataOK
// MbtaV_R1_nEvents_raw
// MbtaV_R1_SNRmax_raw
