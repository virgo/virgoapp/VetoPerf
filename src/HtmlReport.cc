/**
 * @file 
 * @brief See VVetoPerf.h
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "VVetoPerf.h"

////////////////////////////////////////////////////////////////////////////////////
bool VetoPerf::MakeReport(void){
////////////////////////////////////////////////////////////////////////////////////

  // no triggers
  if(TP==NULL){
    cerr<<"VetoPerf::MakeReport: no input triggers"<<endl;
    return false;
  }

  // sort vetoes by decreasing effiency
  vector <unsigned int> vindex = SortVetoes();

  // import material
  string vetoperf_html = getenv("VETOPERF_HTML");
  string gwollum_pix = getenv("GWOLLUM_PIX");
  error_code ec;
  if(!CopyFile(ec, vetoperf_html+"/style.css", outdir+"/style.css", true)){
    cerr<<"VetoPerf::MakeReport: cannot copy style css sheet ("<<ec<<")"<<endl;
  }
  if(!CopyFile(ec, gwollum_pix+"/gwollum_logo_min_trans.gif", outdir+"/icon.gif", true)){
    cerr<<"VetoPerf::MakeReport: cannot copy gwollum icon ("<<ec<<")"<<endl;
  }

  // summary file
  ofstream summary((outdir+"/vp.summary.txt").c_str());
  summary<<"% Veto index "<<endl;
  summary<<"% Stream name of target triggers"<<endl;
  summary<<"% Veto name"<<endl;
  summary<<"% Number of veto segments"<<endl;
  summary<<"% Veto dead-time [s]"<<endl;
  summary<<"% Veto efficiencies [%]"<<endl;

  // index header & scripts
  ofstream report((outdir+"/vp.html").c_str());
  report<<"<html>"<<endl;
  report<<"<head>"<<endl;
  report<<"<title>VetoPerf Report</title>"<<endl;
  report<<"<link rel=\"stylesheet\" href=\"style.css\" type=\"text/css\" />"<<endl;
  report<<"<link rel=\"icon\" type=\"image/x-icon\" href=\"./icon.gif\" />"<<endl;
  report<<"<link rel=\"shortcut icon\" type=\"image/x-icon\" href=\"./icon.gif\" />"<<endl;
  report<<"<script type=\"text/javascript\">"<<endl;
  report<<"function showPlot(target, source, type, allp) {"<<endl;
  report<<"  var basename ="<<endl;
  report<<"    source + \"_\" + type;"<<endl;
  report<<"  if (allp==\"all\") {"<<endl;
  report<<"    document.getElementById(\"plot_\" + source).src ="<<endl;
  report<<"    \"./\" + target + \"-all_\" + type +\".png\";"<<endl;
  report<<"  }"<<endl;
  report<<"  else {"<<endl;
  report<<"    document.getElementById(\"plot_\" + source).src ="<<endl;
  report<<"    \"./\" + target + \"-\" + source + \"_\" + type +\".png\";"<<endl;
  report<<"  }"<<endl;
  report<<"}"<<endl;
  report<<"</script>"<<endl;
  report<<"<script type=\"text/javascript\">"<<endl;
  report<<"function toggle(anId) {"<<endl;
  report<<"  node = document.getElementById(anId);"<<endl;
  report<<"  if (node.style.visibility==\"hidden\") {"<<endl;
  report<<"    node.style.visibility = \"visible\";"<<endl;
  report<<"    node.style.height = \"auto\";"<<endl;
  report<<"  }"<<endl;
  report<<"  else {"<<endl;
  report<<"    node.style.visibility = \"hidden\";"<<endl;
  report<<"    node.style.height = \"0\";"<<endl;
  report<<"  }"<<endl;
  report<<"}"<<endl;
  report<<"</script>"<<endl;
  report<<"</head>"<<endl;
  report<<"<body>"<<endl;
  report<<endl;

  // index title
  report<<"<h1>VetoPerf analysis over "<<TP->GetName()<<" </h1>"<<endl;
  report<<"<hr />"<<endl;
  report<<endl;

  // subtitle
  if(subtitle.compare("")){
    report<<"<pre>"<<subtitle<<"</pre>"<<endl;
    report<<"<hr />"<<endl;
    report<<endl;
  }

  // index summary
  report<<"<h2>Summary</h2>"<<endl;
  tm utc;
  time_t timer_stop;
  time ( &timer_stop );
  struct tm * ptm = gmtime ( &timer_stop );
  char *login = getlogin(); string slogin;
  if(login==NULL) slogin = "unknown";
  else            slogin = (string)login;
  report<<"<table>"<<endl;
  report<<"  <tr><td>VetoPerf version:</td><td>"<<(string)VP_PROJECT_VERSION<<": <a href=\"https://virgo.docs.ligo.org/virgoapp/VetoPerf/\" target=\"_blank\">documentation</a> <a href=\"https://git.ligo.org/virgo/virgoapp/VetoPerf\" target=\"_blank\">gitlab repository</a></td></tr>"<<endl;
  report<<"  <tr><td>VetoPerf run by:</td><td>"<<slogin<<"</td></tr>"<<endl;
  report<<"  <tr><td>VetoPerf processing time:</td><td>"<<(unsigned int)(timer_stop-timer_start)/3600<<" h, "<<((unsigned int)(timer_stop-timer_start)%3600)/60<<" min, "<<(unsigned int)(timer_stop-timer_start)%60<<" s</td></tr>"<<endl;
  report<<"  <tr><td>Processing Date:</td><td>"<<asctime(ptm)<<" (UTC)</td></tr>"<<endl;
  GPSToUTC(&utc, InSeg->GetFirst());
  report<<"  <tr><td>Requested start:</td><td>"<<(unsigned int)InSeg->GetFirst()<<" &rarr; "<<asctime(&utc)<<" (UTC)</td></tr>"<<endl;
  GPSToUTC(&utc, InSeg->GetLast());
  report<<"  <tr><td>Requested stop:</td><td>"<<(unsigned int)InSeg->GetLast()<<" &rarr; "<<asctime(&utc)<<" (UTC)</td></tr>"<<endl;
  report<<"  <tr><td>Requested livetime:</td><td>"<<(unsigned int)InSeg->GetLiveTime()<<" sec &rarr; "<<setprecision(3)<<fixed<<InSeg->GetLiveTime()/3600.0/24<<" days</td></tr>"<<endl;
  InSeg->Dump(2,outdir+"/vp.insegments.txt");
  report<<"  <tr><td>Requested segments:</td><td><a href=\"./vp.insegments.txt\">vp.insegments.txt</a></td></tr>"<<endl;
  report<<"  <tr><td>Summary text file:</td><td><a href=\"./vp.summary.txt\">vp.summary.txt</a></td></tr>"<<endl;
  report<<"</table>"<<endl;
  report<<"<hr />"<<endl;
  report<<endl;

  // triggers summary
  report<<"<h2>Triggers ("<<TP->GetName()<<" - "<<TP->GetProcessName()<<")</h2>"<<endl;
  report<<"<table>"<<endl;
  report<<"  <tr><td>Number of raw triggers:</td><td>"<<TP->GetTriggerN()<<"</td></tr>"<<endl;
  report<<"  <tr><td>Number of raw clusters:</td><td>"<<TP->GetClusterN()<<"</td></tr>"<<endl;
  report<<"  <tr><td>Number of active clusters:</td><td>";
  for(unsigned int s=0; s<snrthr.size(); s++)
    report<<n_c_snr[s]<<" (SNR &gt; "<<snrthr[s]<<") ";
  report<<"</td></tr>"<<endl;
  report<<"</table>"<<endl;
  report<<"<hr />"<<endl;

  // veto summary
  report<<"<h2>Vetoes</h2>"<<endl;
  report<<"<table>"<<endl;
  report<<"  <tr><td>Number of vetoes:</td><td>"<<veto.size()<<"</td></tr>"<<endl;
  report<<"  <tr><td>Dead time:</td><td> Integrated time when the veto is active. We note <i>d</i> the fraction of the total livetime ("<<(unsigned int)InSeg->GetLiveTime()<<" s) when the veto is active.</td></tr>"<<endl;
  report<<"  <tr><td>Efficiency (&epsilon;):</td><td>This is the fraction of "<<TP->GetName()<<" triggers which are vetoed.</td></tr>"<<endl;
  report<<"  <tr><td>&epsilon;/d:</td><td>This factor is larger than 1 when the veto rejects more triggers than a random veto.</td></tr>"<<endl;
  report<<"</table>"<<endl;

  if(summary_snrthr>=0){
    report<<"<table>"<<endl;
    report<<"  <tr>"<<endl;
    report<<"    <td><table>"<<endl;
    report<<"      <tr><th>"<<TP->Streams::GetName()<<": "<<n_c_snr[0]<<" clusters</th></tr>"<<endl;

    // time-frequency plot + legend
    TP->Draw(hfreqtime);
    TP->SetLogx(0); TP->SetLogy(1);
    if(gfreqtime->GetN()) TP->Draw(gfreqtime, "Psame");
    unsigned int vmax = 0;
    for(unsigned int v=0; v<TMath::Min((UInt_t)10, (UInt_t)veto.size()); v++){
      if(veto[vindex[v]]->GetVetoClusterN(0) == 0) continue;
      vmax++;
    }
    for(unsigned int v=0; v<vmax; v++){
      if(veto[vindex[v]]->GetVetoClusterN(0) == 0) continue;
      if(veto[vindex[v]]->GetVetoFreqTime()->GetN()) TP->Draw(veto[vindex[v]]->GetVetoFreqTime(), "Psame");
      veto[vindex[v]]->SetColor(TP->GetColorPalette((int)((double)(vmax-v)/(double)vmax*(double)(TP->GetNumberOfColors()))-2));
      report<<"      <tr><td><a href=\"#V"<<v<<"\"><font color=\""<<gROOT->GetColor(veto[vindex[v]]->GetColor())->AsHexString()<<"\">V"<<v<<" &rarr; "<<veto[vindex[v]]->GetName()<<"</font></a>, vetoed clusters: "<<veto[vindex[v]]->GetVetoClusterN(0)<<" ("<<(double)veto[vindex[v]]->GetVetoClusterN(0)/(double)n_c_snr[0]*100.0<<" %)</td></tr>"<<endl;
    }
    TP->Print(outdir+"/"+TP->GetNameConv()+"-vetoall_freqtime.png");

    // snr-time plot
    TP->Draw(hsnrtime);
    TP->SetLogx(0); TP->SetLogy(1);
    if(gsnrtime->GetN()) TP->Draw(gsnrtime, "Psame");
    for(unsigned int v=0; v<vmax; v++){
      if(veto[vindex[v]]->GetVetoSnrTime()->GetN()) TP->Draw(veto[vindex[v]]->GetVetoSnrTime(), "Psame");
    }
    TP->Print(outdir+"/"+TP->GetNameConv()+"-vetoall_snrtime.png");

    // snr-frequency plot
    TP->Draw(hsnrfreq);
    TP->SetLogx(1); TP->SetLogy(1);
    if(gsnrfreq->GetN()) TP->Draw(gsnrfreq, "Psame");
    for(unsigned int v=0; v<vmax; v++){
      if(veto[vindex[v]]->GetVetoSnrFreq()->GetN()) TP->Draw(veto[vindex[v]]->GetVetoSnrFreq(), "Psame");
    }
    TP->Print(outdir+"/"+TP->GetNameConv()+"-vetoall_snrfreq.png");

    report<<"    </table></td>"<<endl;

    report<<"    <td><a href=\"./"<<TP->GetNameConv()<<"-vetoall_freqtime.png\"><img src=\"./"<<TP->GetNameConv()<<"-vetoall_freqtime.png\" width=\"400\"/></a></td>"<<endl;
    report<<"    <td><a href=\"./"<<TP->GetNameConv()<<"-vetoall_snrtime.png\"><img src=\"./"<<TP->GetNameConv()<<"-vetoall_snrtime.png\" width=\"400\"/></a></td>"<<endl;
    report<<"    <td><a href=\"./"<<TP->GetNameConv()<<"-vetoall_snrfreq.png\"><img src=\"./"<<TP->GetNameConv()<<"-vetoall_snrfreq.png\" width=\"400\"/></a></td>"<<endl;
    report<<"  </tr>"<<endl;
    report<<"</table>"<<endl;
  }
  report<<"<hr />"<<endl;
  report<<endl;

  // loop over vetoes
  double sig_max;
  string color_effdead;
  for(unsigned int v=0; v<veto.size(); v++){

    // get best efficiency/dead-time value (ranking)
    sig_max=0.0;
    for(unsigned int s=0; s<snrthr.size(); s++)
      if((double)(veto[vindex[v]]->GetVetoClusterN(s))*InSeg->GetLiveTime()/veto[vindex[v]]->GetDeadTime()/(double)(n_c_snr[s]) > sig_max)
	sig_max=(double)(veto[vindex[v]]->GetVetoClusterN(s))*InSeg->GetLiveTime()/veto[vindex[v]]->GetDeadTime()/(double)(n_c_snr[s]);
    if(sig_max>100) color_effdead="red";
    else if(sig_max>10) color_effdead="orange";
    else if(sig_max>2) color_effdead="green";
    else color_effdead="black";

    // veto header
    report<<"<h2><a name=\"V"<<v<<"\"></a><font color=\""<<gROOT->GetColor(veto[vindex[v]]->GetColor())->AsHexString()<<"\">V"<<v<<":</font> "<<veto[vindex[v]]->GetName()<<" (&epsilon; = "<<(double)(veto[vindex[v]]->GetVetoClusterN(0))/(double)n_c_snr[0]*100.0<<"%, <font color=\""<<color_effdead<<"\">&epsilon;/d = "<<sig_max<<"</font>) <a href=\"javascript:void(0)\" name=\""<<veto[vindex[v]]->GetName()<<"\" onclick=\"toggle('src_"<<veto[vindex[v]]->GetName()<<"')\">[click here to expand/hide]</a> <a href=\"#V"<<v<<"\">[link to here]</a></h2>"<<endl;
    report<<"<div id=\"src_"<<veto[vindex[v]]->GetName()<<"\" style=\"visibility:hidden;height:0;\">"<<endl;
    report<<"  <table>"<<endl;
    report<<"    <tr>"<<endl;

    report<<"      <td>"<<endl;

    // veto segments section
    report<<"        <table class=\"list\">"<<endl;
    gwl_ss<<"./veto"<<vindex[v]<<".txt";
    if(printveto)
      report<<"          <tr><th>Veto:</th><th><a href=\""<<gwl_ss.str()<<"\">text file</a></th><th></th></tr>"<<endl;
    else
      report<<"          <tr><th>Veto:</th><th></th><th></th></tr>"<<endl;
    gwl_ss.clear(); gwl_ss.str("");
    report<<"          <tr><td>Number of veto segments:</td><td>"<<veto[vindex[v]]->GetVetoSegmentN()<<"</td><td></td></tr>"<<endl;
    report<<"          <tr><td>Veto dead-time:</td><td>"<<veto[vindex[v]]->GetDeadTime()<<" s</td><td>("<<veto[vindex[v]]->GetDeadTime()/InSeg->GetLiveTime()*100.0<<" %)</td></tr>"<<endl;
    gwl_ss<<"./veto"<<vindex[v]<<"-clusters.txt";
    if(printvetoclusters)
      report<<"          <tr><td>Number of vetoed target clusters ("<<TP->GetName()<<"):</td><td>"<<veto[vindex[v]]->GetVetoClusterN(0)<<"</td><td><a href=\""<<gwl_ss.str()<<"\">text file</a></td></tr>"<<endl;
    else
      report<<"          <tr><td>Number of vetoed clusters:</td><td>"<<veto[vindex[v]]->GetVetoClusterN(0)<<"</td><td></td></tr>"<<endl;
    gwl_ss.clear(); gwl_ss.str("");
    report<<"        </table>"<<endl;

    // performance section
    report<<"        <table class=\"list\">"<<endl;
    report<<"          <tr><th>Performance on "<<TP->GetName()<<" clusters:</th></tr>"<<endl;
    report<<"        </table>"<<endl;

    report<<"        <table class=\"perflist\">"<<endl;
    report<<"          <tr>"<<endl;
    report<<"            <th></th>"<<endl;
    for(unsigned int s=0; s<snrthr.size(); s++)
      report<<"            <th colspan=\"2\">SNR &gt; "<<snrthr[s]<<"</th>"<<endl;
    report<<"          </tr>"<<endl;

    report<<"          <tr>"<<endl;
    report<<"            <td>Number of vetoed clusters:</td>"<<endl;
    for(unsigned int s=0; s<snrthr.size(); s++)
      report<<"            <td align=\"right\">"<<veto[vindex[v]]->GetVetoClusterN(s)<<"/"<<n_c_snr[s]<<"</td><td align=\"right\">("<<(double)(veto[vindex[v]]->GetVetoClusterN(s))/(double)n_c_snr[s]*100.0<<"%)</td>"<<endl;
    report<<"          </tr>"<<endl;

    report<<"          <tr>"<<endl;
    report<<"            <td>Number of used veto segments:</td>"<<endl;
    for(unsigned int s=0; s<snrthr.size(); s++)
      report<<"            <td>"<<veto[vindex[v]]->GetVetoUseSegmentN(s)<<"</td><td>("<<(double)(veto[vindex[v]]->GetVetoUseSegmentN(s))/(double)(veto[vindex[v]]->GetVetoSegmentN())*100.0<<"%)</td>"<<endl;
    report<<"          </tr>"<<endl;

    report<<"        </table>"<<endl;

    // summary
    summary<<setprecision(5)
           <<v<<" "
           <<TP->Streams::GetName()<<" "
           <<veto[vindex[v]]->GetName()<<" "
           <<veto[vindex[v]]->GetVetoSegmentN()<<" "
           <<veto[vindex[v]]->GetDeadTime()<<" "
           <<(double)(veto[vindex[v]]->GetVetoClusterN(0))/(double)n_c_snr[0]*100.0;
    for(unsigned int s=1; s<snrthr.size(); s++)
      summary<<"/"<<(double)(veto[vindex[v]]->GetVetoClusterN(s))/(double)n_c_snr[s]*100.0;
    summary<<endl;

    // Plot section
    report<<"        <table class=\"list\">"<<endl;
    report<<"          <tr><th>Plots:</th><th></th></tr>"<<endl;

    // SNR plot
    if(veto[vindex[v]]->GetVetoClusterN(0) > 0)
      report<<"          <tr><td>&rarr; <a href=\"javascript:showPlot('"<<TP->GetNameConv()<<"', 'veto"<<vindex[v]<<"', 'snr','toto');\">SNR distribution before and after veto</a></td><td></td></tr>"<<endl;

    // time-frequency plot
    if((summary_snrthr>=0) && (veto[vindex[v]]->GetVetoFreqTime()->GetN())){
      TP->Draw(hfreqtime);
      TP->SetLogx(0); TP->SetLogy(1);
      TP->Draw(gfreqtime, "Psame");
      TP->Draw(veto[vindex[v]]->GetVetoFreqTime(), "Psame");
      gwl_ss<<outdir+"/"+TP->GetNameConv()+"-veto"+vindex[v]+"_freqtime_a.png";
      TP->Print(gwl_ss.str());
      gwl_ss.clear(); gwl_ss.str("");
      report<<"          <tr><td>&rarr; <a href=\"javascript:showPlot('"<<TP->GetNameConv()<<"', 'veto"<<vindex[v]<<"', 'freqtime_a','toto');\">Time-frequency trigger distribution</a>:</td><td><a href=\"javascript:showPlot('"<<TP->GetNameConv()<<"', 'veto"<<vindex[v]<<"', 'freqtime','all');\">before</a>/<a href=\"javascript:showPlot('"<<TP->GetNameConv()<<"', 'veto"<<vindex[v]<<"', 'freqtime','toto');\">after</a> veto</td></tr>"<<endl;
    }
    else{
      if(veto[vindex[v]]->GetVetoClusterN(0) > 0)
        report<<"          <tr><td>&rarr; Time-frequency trigger distribution:</td><td><a href=\"javascript:showPlot('"<<TP->GetNameConv()<<"', 'veto"<<vindex[v]<<"', 'freqtime','all');\">before</a>/<a href=\"javascript:showPlot('"<<TP->GetNameConv()<<"', 'veto"<<vindex[v]<<"', 'freqtime','toto');\">after</a> veto</td></tr>"<<endl;
      else
        report<<"          <tr><td>&rarr; Time-frequency trigger distribution:</td><td><a href=\"javascript:showPlot('"<<TP->GetNameConv()<<"', 'veto"<<vindex[v]<<"', 'freqtime','all');\">plot</a></td></tr>"<<endl;
    }

    // SNR-time
    if((summary_snrthr>=0) && (veto[vindex[v]]->GetVetoSnrTime()->GetN())){
      TP->Draw(hsnrtime);
      TP->SetLogx(0); TP->SetLogy(1);
      TP->Draw(gsnrtime, "Psame");
      TP->Draw(veto[vindex[v]]->GetVetoSnrTime(), "Psame");
      gwl_ss<<outdir+"/"+TP->GetNameConv()+"-veto"+vindex[v]+"_snrtime_a.png";
      TP->Print(gwl_ss.str());
      gwl_ss.clear(); gwl_ss.str("");
      report<<"          <tr><td>&rarr; <a href=\"javascript:showPlot('"<<TP->GetNameConv()<<"', 'veto"<<vindex[v]<<"', 'snrtime_a','toto');\">SNR-time trigger distribution</a>:</td><td><a href=\"javascript:showPlot('"<<TP->GetNameConv()<<"', 'veto"<<vindex[v]<<"', 'snrtime','all');\">before</a>/<a href=\"javascript:showPlot('"<<TP->GetNameConv()<<"', 'veto"<<vindex[v]<<"', 'snrtime','toto');\">after</a> veto</td></tr>"<<endl;
    }
    else{
      if(veto[vindex[v]]->GetVetoClusterN(0) > 0)
        report<<"          <tr><td>&rarr; SNR-time trigger distribution:</td><td><a href=\"javascript:showPlot('"<<TP->GetNameConv()<<"', 'veto"<<vindex[v]<<"', 'snrtime','all');\">before</a>/<a href=\"javascript:showPlot('"<<TP->GetNameConv()<<"', 'veto"<<vindex[v]<<"', 'snrtime','toto');\">after</a> veto</td></tr>"<<endl;
      else
        report<<"          <tr><td>&rarr; SNR-time trigger distribution:</td><td><a href=\"javascript:showPlot('"<<TP->GetNameConv()<<"', 'veto"<<vindex[v]<<"', 'snrtime','all');\">plot</a></td></tr>"<<endl;
    }

    // SNR-frequency
    if((summary_snrthr>=0) && (veto[vindex[v]]->GetVetoSnrFreq()->GetN())){
      TP->Draw(hsnrfreq);
      TP->SetLogx(1); TP->SetLogy(1);
      TP->Draw(gsnrfreq, "Psame");
      TP->Draw(veto[vindex[v]]->GetVetoSnrFreq(), "Psame");
      gwl_ss<<outdir+"/"+TP->GetNameConv()+"-veto"+vindex[v]+"_snrfreq_a.png";
      TP->Print(gwl_ss.str());
      gwl_ss.clear(); gwl_ss.str("");
      report<<"          <tr><td>&rarr; <a href=\"javascript:showPlot('"<<TP->GetNameConv()<<"', 'veto"<<vindex[v]<<"', 'snrfreq_a','toto');\">SNR-frequency trigger distribution</a>:</td><td><a href=\"javascript:showPlot('"<<TP->GetNameConv()<<"', 'veto"<<vindex[v]<<"', 'snrfreq','all');\">before</a>/<a href=\"javascript:showPlot('"<<TP->GetNameConv()<<"', 'veto"<<vindex[v]<<"', 'snrfreq','toto');\">after</a> veto</td></tr>"<<endl;
    }
    else{
      if(veto[vindex[v]]->GetVetoClusterN(0) > 0)
        report<<"          <tr><td>&rarr; SNR-frequency trigger distribution:</td><td><a href=\"javascript:showPlot('"<<TP->GetNameConv()<<"', 'veto"<<vindex[v]<<"', 'snrfreq','all');\">before</a>/<a href=\"javascript:showPlot('"<<TP->GetNameConv()<<"', 'veto"<<vindex[v]<<"', 'snrfreq','toto');\">after</a> veto</td></tr>"<<endl;
      else
        report<<"          <tr><td>&rarr; SNR-frequency trigger distribution:</td><td><a href=\"javascript:showPlot('"<<TP->GetNameConv()<<"', 'veto"<<vindex[v]<<"', 'snrfreq','all');\">plot</a></td></tr>"<<endl;
    }
    
    report<<"        </table>"<<endl;
    report<<"      </td>"<<endl;

    // plot area
    report<<"      <td>"<<endl;
    if(veto[vindex[v]]->GetVetoClusterN(0) > 0)
      report<<"        <img id=\"plot_veto"<<vindex[v]<<"\" src=\"./"<<TP->GetNameConv()<<"-veto"<<vindex[v]<<"_snr.png\" />"<<endl;
    else
      report<<"        <img id=\"plot_veto"<<vindex[v]<<"\" src=\"./"<<TP->GetNameConv()<<"-all_freqtime.png\" />"<<endl;

    report<<"      </td>"<<endl;

    report<<"    </tr>"<<endl;
    report<<"  </table>"<<endl;
    report<<"</div>"<<endl;
    report<<"<hr/>"<<endl;
    report<<endl;

  }
  
  // index footer
  report<<"<table>"<<endl;
  report<<"  <tr><td>Florent Robinet, <a href=\"mailto:florent.robinet@ijclab.in2p3.fr\">florent.robinet@ijclab.in2p3.fr</a></td></tr>"<<endl;
  report<<"</table>"<<endl;
  report<<"</body>"<<endl;
  report<<"</html>"<<endl;
  report.close();
  summary.close();

  return true;
}

////////////////////////////////////////////////////////////////////////////////////
vector<unsigned int> VetoPerf::SortVetoes(void){
////////////////////////////////////////////////////////////////////////////////////

  vector<unsigned int> vindex;

  // no vetoes
  if(veto.size()==0) return vindex;

  vector <unsigned int> vn;
  bool sorted;

  // init
  vindex.push_back(0);
  vn.push_back(veto[0]->GetVetoClusterN(0));

  // loop over vetoes
  for(unsigned int v=1; v<veto.size(); v++){

    // not sorted
    sorted=false;

    // loop over previously sorted vetoes
    for(unsigned int i=0; i<vindex.size(); i++){

      // this veto is better
      if(veto[v]->GetVetoClusterN(0)>=vn[i]){

        // insert it in the list
        vindex.insert(vindex.begin()+i, v);

        // save N vetoed cluster
 	vn.insert(vn.begin()+i, veto[v]->GetVetoClusterN(0));
 	sorted=true;
 	break;
      }
    }

    // not better: append to the list
    if(!sorted){
      vindex.push_back(v);
      vn.push_back(veto[v]->GetVetoClusterN(0));
    }
  }

  vn.clear();

  return vindex;
}
